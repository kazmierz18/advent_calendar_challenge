/*===============*/
/* Day 15        */
/*===============*/
#include <array>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

struct Generator {
  int factor;
  int value;
  Generator(const int factor, const int start) : factor(factor), value(start) {}
  int next() { value = value * factor; }
};

int main(int argc, char **argv) {
  // read input file
  char *name = nullptr;
  if (argc >= 2) {
    name = argv[1];
  } else {
    return 1;
  }
  std::ifstream file(name);
  std::string tmp;
  std::vector<std::string> lines;
  while (std::getline(file, tmp)) {
    // remove new line characters
    if (*(tmp.end()) == '\n') {
      tmp.pop_back();
    }
    lines.push_back(tmp);
  }
  std::array<int, 2> start_values;
  auto i = 0;
  for (const auto &line : lines) {
    std::string number = line.substr(24, line.size() - 24);
    start_values[i] = std::atoi(number.c_str());
    i++;
  }

  //================================================
  Generator A(16807, start_values[0]);
  Generator B(48271, start_values[1]);
  std::cout << "Part One: "
            << "\n";
  //================================================
  std::cout << "Part Two: "
            << "\n";
  return 0;
}
