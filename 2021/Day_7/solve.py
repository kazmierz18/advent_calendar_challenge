"""Day 7 problem solver"""
import matplotlib.pyplot as plt
import numpy as np
import math


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def main():
    lines = read_lines("Day_7/inputs/input1")
    if lines is None:
        return 1
    positions = [int(x) for x in (lines[0].split(","))]

    # plt.plot(positions)
    # plt.show()

    avg = sum(positions)//len(positions)
    std_dev = math.ceil(np.std(positions))
    fuels_c = {}
    for x in range(avg-std_dev, avg+std_dev, 1):
        f = sum([(abs(x-y)) for y in positions])
        fuels_c[x] = f

    plt.plot(fuels_c.keys(), fuels_c.values())
    plt.grid(True)
    plt.show()
    m = min(fuels_c.values())

    ###############################################
    # Part One
    print("Part 1: %d" % m)
    ###############################################
    # Part Two

    fuels_c = {}
    for x in range(avg-std_dev, avg+std_dev, 1):
        f = sum([(1+abs(x-y))*(abs(x-y))/2 for y in positions])
        fuels_c[x] = f

    plt.plot(fuels_c.keys(), fuels_c.values())
    plt.grid(True)
    plt.show()
    # m = min(fuels_c, key=fuels_c.get)
    m = min(fuels_c.values())
    print("Part 2: %d" % m)
    return 0


main()
