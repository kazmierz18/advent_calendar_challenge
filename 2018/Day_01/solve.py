"""Day 1 problem solver"""

from itertools import accumulate


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def main():
    lines = read_lines("Day_01/inputs/input1")
    if lines is None:
        return 1

    #########################################
    # Part One
    result = 0
    changes = list(map(int, lines))
    result_vector = [0]+list(accumulate(changes))
    ret = result_vector[-1]

    print(f"Part One: {ret}")

    ##########################################
    # Part Two
    not_found = True
    current_change_idx = 0
    current_val = ret
    while not_found:
        if current_change_idx >= len(changes):
            current_change_idx = 0
        current_val += changes[current_change_idx]
        for res in result_vector:
            if res == current_val:
                print("Part Two: ", res)
                not_found = False
                break
        current_change_idx += 1
    return 0


main()
