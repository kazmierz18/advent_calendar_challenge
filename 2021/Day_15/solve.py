"""Day 15 problem solver"""

import numpy as np
from heapq import heappush, heappop, heapify
from collections import namedtuple
import sys
from matplotlib import pyplot as plt
import math


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


Point = namedtuple("Point", ['x', 'y'])


def ny_distance(p1: Point, p2: Point):
    return abs(p1.x-p2.x)+abs(p1.y-p2.y)


def euler_dist(p1: Point, p2: Point):
    newX = abs(p1.x-p2.x)
    newY = abs(p1.y-p2.y)
    return math.sqrt(newX*newX+newY*newY)


def a_star(grid, start: Point, end: Point, heuristic):
    dist = np.full(grid.shape, sys.maxsize)
    visited = np.zeros(grid.shape)
    path = np.zeros(grid.shape)
    dist[start.y][start.x] = 0
    q = [(0, 0, start, [])]
    heapify(q)
    visited = set()

    def validPointValidator(shape):
        sx, sy = shape

        def check(p: Point):
            return p.x >= 0 and p.x < sx and p.y >= 0 and p.y < sy
        return check
    isValidPoint = validPointValidator(grid.shape)
    best_path = None
    op = 0
    while q:
        f, current_distance, p, path = heappop(q)
        visited.add(p)
        neighbors = [Point(0, 1), Point(0, -1), Point(1, 0), Point(-1, 0)]

        if p.x == end.x-1 and p.y == end.y-1:
            best_path = path
            break

        valid_neighbors = list(filter(
            isValidPoint, [Point(p.x+pn.x, p.y+pn.y) for pn in neighbors]))

        for n in valid_neighbors:
            if n in visited:
                continue
            op += 1
            d = grid[n.y][n.x] + current_distance
            if d < dist[n.y][n.x]:
                dist[n.y][n.x] = d
                f = heuristic(n, end)*1+d
                f = d
                heappush(q, (f, d, n, path+[n]))

    print(op)
    plt.imshow(dist)
    plt.show()
    path = np.zeros(grid.shape)
    for n in best_path:
        path[n.y][n.x] = 100
    plt.imshow(path)
    plt.show()
    return best_path

def create_large_cave(cave):
    old = np.copy(cave)
    result = np.copy(cave)
    for i in range(4):
        new_cols = old + np.ones(cave.shape, dtype=int)
        new_cols = np.where(new_cols > 9, 1, new_cols)
        result = np.c_[result,new_cols]
        old = result[:,-cave.shape[0]:]
    old = result
    for i in range(4):
        new_cols = old + np.ones((cave.shape[0], cave.shape[1]*5), dtype=int)
        new_cols = np.where(new_cols > 9, 1, new_cols)
        result = np.r_[result,new_cols]
        old = result[-cave.shape[0]:,:]    
    return(result)


def main():
    grid = np.genfromtxt("Day_15/inputs/input1", delimiter=1, dtype=int)
    # grid = np.full((1,1),8)
    ###############################################
    # Part One
    ret = 0
    start = Point(0, 0)
    end = Point(grid.shape[1], grid.shape[0])
    path = a_star(grid, start, end, ny_distance)
    for n in path:
        ret += grid[n.y][n.x]
    print(f"Part 1: {ret}")
    ###############################################
    # Part Two
    grid = create_large_cave(grid)
    end = Point(grid.shape[1], grid.shape[0])
    path = a_star(grid, start, end, ny_distance)
    ret = 0
    for n in path:
        ret += grid[n.y][n.x]
    print(f"Part 2: {ret}")
    return 0


main()
