"""Day 5 problem solver"""
import re
import matplotlib.pyplot as plt
import numpy as np
from collections import namedtuple
from functools import reduce


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


Point = namedtuple('Point', ['x', 'y'])
Line = namedtuple('Line', ['p1', 'p2'])


def main():
    lines = read_lines("Day_5/inputs/input1")
    if lines is None:
        return 1
    ###############################################
    # Part One
    i_lines = []
    max_x = 0
    max_y = 0
    for line in lines:
        p1, p2 = line.split('->')
        x1, y1 = p1.split(',')
        p1 = Point(int(x1), int(y1))
        x2, y2 = p2.split(',')
        p2 = Point(int(x2), int(y2))
        i_lines.append(Line(p1, p2))
    max_X = max([l.p1.x for l in i_lines]+[l.p2.x for l in i_lines])
    max_Y = max([l.p1.y for l in i_lines]+[l.p2.y for l in i_lines])

    map_image = np.zeros((max_x+1, max_y+1))
    for l in i_lines:
        if l.p1.x == l.p2.x:  # vertical x is const
            length = abs(l.p1.x-l.p2.x)+1
            start = min([l[1], l[3]])
            for dy in range(length):
                x = l[0]
                y = start + dy
                map_image[y][x] += 1
        elif l.p1.y == l.p2.y:  # horizontal y is const
            length = abs(l.p1.y-l.p2.y)+1
            start = min([l[0], l[2]])
            for dx in range(length):
                x = start + dx
                y = l[1]
                map_image[y][x] += 1

    sum = 0
    for x in range(map_image.shape[0]):
        for y in range(map_image.shape[1]):
            if map_image[y][x] > 1:
                sum += 1
    print("Part 1: %d" % sum)
    ###############################################
    # Part Two
    for l in i_lines:
        if l[0] == l[2]:  # vertical x is const
            pass
        elif l[1] == l[3]:  # horizontal y is const
            pass
        else:
            length_x = (l[0]-l[2])
            length_y = (l[1]-l[3])
            tmp = []
            if length_x > 0 and length_y > 0:
                start_x = min([l[0], l[2]])
                start_y = min([l[1], l[3]])
                tmp = [(a, a) for a in range(abs(abs(length_x)+1))]
            elif length_x < 0 and length_y < 0:
                start_x = max([l[0], l[2]])
                start_y = max([l[1], l[3]])
                tmp = [(-a, -a) for a in range(abs(abs(length_x)+1))]
            elif length_x > 0 and length_y < 0:
                start_x = min([l[0], l[2]])
                start_y = max([l[1], l[3]])
                tmp = [(a, -a) for a in range(abs(abs(length_x)+1))]
            elif length_x < 0 and length_y > 0:
                start_x = max([l[0], l[2]])
                start_y = min([l[1], l[3]])
                tmp = [(-a, a) for a in range(abs(abs(length_x)+1))]
            for dx, dy in tmp:
                x = start_x + dx
                y = start_y + dy
                map_image[y][x] += 1

    plt.imshow(map_image)
    plt.show()
    ret = 0
    for x in range(map_image.shape[0]):
        for y in range(map_image.shape[1]):
            if map_image[y][x] > 1:
                ret += 1
    print(f"Part 2: {ret}")
    return 0


main()
