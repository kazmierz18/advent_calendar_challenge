"""Day 13 problem solver"""

from intcode import intcode_cpu, preprocess
from queue import Queue
from threading import Thread


class CPU_thread(Thread):
    def __init__(self, cpu, id):
        Thread.__init__(self)
        self.id = id
        self.cpu = cpu

    def run(self):
        self.cpu.run()


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


EMPTY, WALL, BLOCK, HORIZONTAL_PADDLE, BALL = 0, 1, 2, 3, 4


def print_map(map):
    for line in map:
        for p in line:
            if p == WALL:
                print('#', end="")
            if p == BLOCK:
                print('Q', end="")
            if p == HORIZONTAL_PADDLE:
                print('_', end="")
            if p == BALL:
                print('o', end="")
            if p == EMPTY:
                print(' ', end="")
        print()


def main():
    lines = read_lines("Day_13/inputs/input1")
    if lines is None:
        return 1
    program = preprocess(lines)
    for _ in range(200):
        program.append(0)
    ###############################################
    # Part One
    cpu = intcode_cpu()
    cpu.load_program(program)
    cpu.input = Queue(3)
    cpu.output = Queue(3)
    th = CPU_thread(cpu, 0)
    HEIGHT = 20
    WIDTH = 40
    map = [[0 for _ in range(WIDTH)] for _ in range(HEIGHT)]
    th.start()
    while th.is_alive():
        try:
            x = cpu.output.get(timeout=4)
        except:
            break
        y = cpu.output.get()
        type = cpu.output.get()
        map[y][x] = type

    print_map(map)
    cnt = 0
    for line in map:
        for p in line:
            if p == BLOCK:
                cnt += 1

    print("Part 1: "+str(cnt))
    ###############################################
    # Part Two
    cpu = intcode_cpu()
    program[0] = 2
    cpu.load_program(program)
    cpu.input = Queue(6)
    cpu.output = Queue(3)
    th = CPU_thread(cpu, 0)
    HEIGHT = 20
    WIDTH = 40
    map = [[0 for _ in range(WIDTH)] for _ in range(HEIGHT)]
    th.start()
    score = 0
    ball=0
    joystic=0
    paddle=0
    while th.is_alive():
        try:
            x = cpu.output.get(timeout=4)
        except:
            break
        y = cpu.output.get()
        type = cpu.output.get()
        if x == -1:
            # print_map(map)
            score = type
            continue
        if type == BALL:
            ball = x
            joystic = 0
            if paddle > ball:
                joystic = -1
            elif paddle < ball:
                joystic = 1
            cpu.input.put(joystic)
        elif type == HORIZONTAL_PADDLE:
            paddle = x

        map[y][x] = type

    print_map(map)
    print(score)
    print("Part 2: "+str(score))
    return 0


main()
