"""Day 7 problem solver"""

import copy


class Part:
    def __init__(self, name):
        self.name = name
        self.time = ord(name)-ord('A')+1+60
        self.requires = []

    def add_requirement(self, part):
        self.requires.append(copy.deepcopy(part))

    def requirement_fullfiled(self, name):
        for idx, req in enumerate(self.requires):
            if req.name == name:
                del self.requires[idx]
                return

    def can_be_done(self):
        return False if len(self.requires) > 0 else True


class Worker:
    anyone_working = 0

    def __init__(self, id):
        self.id = id
        self.time_till_end = 0
        self.process_name = '.'

    def assign_process(self, p):
        self.time_till_end = p.time
        self.process_name = p.name
        Worker.anyone_working += 1

    def work(self):
        if self.time_till_end == 1:
            Worker.anyone_working -= 1
        if self.time_till_end > 0:
            self.time_till_end -= 1

    def is_doing_nothing(self):
        if self.time_till_end == 0:
            return True
        else:
            return False


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def main():
    lines = read_lines("Day_07/inputs/input1")
    if lines is None:
        return 1

    parts = []
    for line in lines:
        splits = line.split(' ')
        name = splits[7]
        p = next((g for g in parts if name == g.name), None)
        if p is None:
            parts.append(Part(name))
            p = parts[-1]
        name = splits[1]
        req = next((x for x in parts if name == x.name), None)
        if req is None:
            parts.append(Part(name))
            req = parts[-1]
        p.add_requirement(req)

    parts2 = copy.deepcopy(parts)
    ###############################################
    # Part One

    ## debuging list all dependency beetwen parts
    # parts.sort(key=lambda x: x.name)
    # for part in parts:
    #     print(part.name, ' <= ', end='')
    #     for req in part.requires:
    #         print(req.name, end='')
    #     print()
    # print()

    done = []
    length = len(parts)
    while len(done) != length:
        can_be_done = []
        for part in parts:
            if part.can_be_done():
                can_be_done.append(part)
        can_be_done.sort(key=lambda x: x.name)
        # debuging show very simple 'graph' of requirements
        # for x in can_be_done:
        #     print(x.name,end='')
        # print()
        done.append(can_be_done[0].name)
        parts.remove(can_be_done[0])
        for part in parts:
            part.requirement_fullfiled(done[-1])

    print()
    print("Part 1: ", ''.join(done))
    print()

    ###############################################
    # Part Two
    total_time = 0
    workers = []
    WORKERS_NUM = 5
    for i in range(WORKERS_NUM):
        workers.append(Worker(i))
    prev_txt = ''
    while len(parts2) > 0 or Worker.anyone_working > 0:
        txt = ''
        # find workers doing nothing
        for worker in workers:
            if worker.is_doing_nothing():
                if worker.process_name != '.':
                    # remove part from required list, because its done
                    for part in parts2:
                        part.requirement_fullfiled(worker.process_name)

        # find jobs that can be done now
        can_be_done = []
        for part in parts2:
            if part.can_be_done():
                can_be_done.append(part)
        can_be_done.sort(key=lambda x: x.name)

        # assign jobs to free workers
        for worker in workers:
            if worker.is_doing_nothing():
                if len(can_be_done) > 0:
                    worker.assign_process(can_be_done[0])
                    parts2.remove(can_be_done[0])
                    del can_be_done[0]
                else:
                    worker.process_name = '.'

        # simulate working
        for worker in workers:
            worker.work()
            txt += worker.process_name

        # debugging trace
        # if txt != prev_txt:
        #     print('{:3d}'.format(total_time)+" "+txt)
        #     prev_txt = txt

        # count time
        total_time += 1
    print("Part 2: ", total_time)
    return 0


main()
