"""Day 15 problem solver"""
from intcode import intcode_cpu, preprocess
from queue import Queue
from threading import Thread
import random


class CPU_thread(Thread):
    def __init__(self, cpu, id):
        Thread.__init__(self)
        self.id = id
        self.cpu = cpu

    def run(self):
        self.cpu.run()


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


EMPTY, WALL, PASSAGE, OXYGEN = -1, 0, 1, 2
NORTH, SOUTH, WEST, EAST = 1, 2, 3, 4


def get_pos(current, dir):
    if dir == NORTH:
        return (current[0], current[1]-1)
    if dir == SOUTH:
        return (current[0], current[1]+1)
    if dir == EAST:
        return (current[0]+1, current[1])
    if dir == WEST:
        return (current[0]-1, current[1])


def print_map(map):
    for line in map:
        for p in line:
            if p == WALL:
                print('#', end="")
            if p == PASSAGE:
                print('.', end="")
            if p == EMPTY:
                print(' ', end="")
            if p == OXYGEN:
                print('O', end="")
        print()


def print_meta_map(map):
    for line in map:
        for p in line:
            print(str(p), end="")
        print()

def main():
    lines = read_lines("Day_15/inputs/input1")
    if lines is None:
        return 1
    program = preprocess(lines)
    ###############################################
    # Part One
    cpu = intcode_cpu()
    cpu.load_program(program)
    cpu.input = Queue(3)
    cpu.output = Queue(3)
    th = CPU_thread(cpu, 0)
    HEIGHT = 50
    WIDTH = 50
    map = [[EMPTY for _ in range(WIDTH)] for _ in range(HEIGHT)]
    meta_map = [[0 for _ in range(WIDTH)] for _ in range(HEIGHT)]
    th.start()
    position = (WIDTH//2, HEIGHT//2)
    prev_position = position
    dir = WEST
    position = get_pos(position, dir)
    cpu.input.put(dir)
    cnt = 0
    while th.is_alive():
        cnt += 1
        try:
            ret = cpu.output.get(timeout=4)
        except:
            break
        if map[position[1]][position[0]] == EMPTY:
            meta_map[prev_position[1]][prev_position[0]] += 1
        if ret == OXYGEN:
            map[position[1]][position[0]] = OXYGEN
            break
        if ret == WALL:
            map[position[1]][position[0]] = WALL
            position = prev_position
        if ret == PASSAGE:
            map[position[1]][position[0]] = PASSAGE
        if cnt % 100 == 0:
            print_map(map)
            print_meta_map(meta_map)
        prev_position = position
        position = get_pos(position, dir)

        if map[position[1]][position[0]] == PASSAGE:
            position = prev_position
            dir = random.choice([NORTH, SOUTH, EAST, WEST])
            position = get_pos(position, dir)
        while map[position[1]][position[0]] == WALL:
            position = prev_position
            dir = random.choice([NORTH, SOUTH, EAST, WEST])
            position = get_pos(position, dir)
        cpu.input.put(dir)
    th.join()
    position = (WIDTH//2, HEIGHT//2)
    map[position[1]][position[0]] = OXYGEN
    print_map(map)
    print("Part 1: ")
    ###############################################
    # Part Two
    print("Part 2: ")
    return 0


main()
