/*===============*/
/* Day 02        */
/*===============*/
#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#define INPUT "../inputs/input1"

int main(int argc, char **argv) {
  // read input file
  char *name = nullptr;
  if (argc >= 2) {
    name = argv[1];
  } else {
    return 1;
  }
  std::ifstream file(name);
  std::string tmp;
  std::vector<std::string> lines;
  while (std::getline(file, tmp)) {
    // remove new line characters
    if (*(tmp.end()) == '\n') {
      tmp.pop_back();
    }
    lines.push_back(tmp);
  }
  //================================================
  auto checksum = 0;
  auto checksum2 = 0;
  for (const auto &line : lines) {
    std::vector<int> row;
    auto currentIdx = -1;
    do {
      currentIdx++;
      const auto foundIdx = line.find_first_of("\t ", currentIdx);
      const auto val = std::atoi(line.substr(currentIdx, foundIdx-currentIdx).c_str());
      row.push_back(val);
      currentIdx = foundIdx;
    } while (currentIdx != std::string::npos);
    auto const maxVal = std::max_element(row.begin(), row.end());
    auto const minVal = std::min_element(row.begin(), row.end());
    checksum += (*maxVal - *minVal);

    std::sort(row.begin(),row.end(), std::greater<>());
    for (auto number = row.begin(); number != row.end(); number++) {
      for (auto compare = number + 1; compare != row.end(); compare++) {
        if (*number % *compare == 0) {
          checksum2 += *number / *compare;
        }
      }
    }
  }
  std::cout << "Part One: " << checksum << "\n";
  //================================================
  std::cout << "Part Two: " << checksum2 << "\n";
  return 0;
}
