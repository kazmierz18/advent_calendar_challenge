"""Day 24 problem solver"""

import re
import copy
import itertools


class Group:
    draw = False

    def __init__(self, n_units, HP, AP, initiative, weakness, immunes, attack_type, side, id):
        self.n_units = n_units
        self.HP = HP
        self.AP = AP
        self.initiative = initiative
        self.weakness = weakness.split(', ') if weakness else []
        self.immunes = immunes.split(', ') if immunes else []
        self.attack_type = attack_type
        self.attacker = None
        self.target = None
        self.side = side
        self.id = id

    def attack(self):
        if self.target is not None:
            attack = self.effective_power*self.bonus(self.target)
            killed_units = attack // self.target.HP
            Group.draw = False
            # print(self.side, self.id, 'attacks',
            #       self.target.id, 'killing', killed_units)
            self.target.n_units = max(0, self.target.n_units-killed_units)

    def regroup(self):
        self.target = None
        self.attacker = None

    @property
    def effective_power(self):
        return self.n_units*self.AP

    def bonus(self, target):
        if self.attack_type in target.immunes:
            return 0
        if self.attack_type in target.weakness:
            return 2
        return 1

    def choose(self, groups):
        if self.target is None:
            targets = [g for g in groups if g.side != self.side and
                       g.attacker is None and self.effective_power*self.bonus(g) > 0]
            # for t in targets:
            #     print(self.side, self.id, 'would deal', t.id, self.effective_power()*self.bonus(t),t.effective_power(), t.initiative)
            if targets:
                # chose group you would deal most damage, then consider target effective power then initiative
                self.target = max(targets, key=lambda g: (self.effective_power*self.bonus(g),
                                                          g.effective_power, g.initiative))
                self.target.attacker = self


def skirmish(army1, army2):
    groups = army1+army2
    groups.sort(key=lambda x: (
        x.effective_power, x.initiative), reverse=True)
    for g in groups:
        g.regroup()
    for g in groups:
        g.choose(groups)
    # print()
    groups.sort(key=lambda x: x.initiative, reverse=True)
    for g in groups:
        if g.target:
            g.attack()


def war(army1, army2, boost=0):
    for a in army1:
        a.AP += boost
    a1 = sum(a.n_units for a in army1)
    a2 = sum(a.n_units for a in army2)
    Group.draw = False
    while a1 != 0 and a2 != 0 and not Group.draw:
        Group.draw = True
        # print('Immune')
        # for a in army1:
        #     print(a.id, a.n_units)
        # print('Infection')
        # for a in army2:
        #     print(a.id, a.n_units)
        # print()
        skirmish(army1, army2)
        # remove dead groups
        army1 = [a for a in army1 if a.n_units > 0]
        army2 = [a for a in army2 if a.n_units > 0]
        a1 = sum(a.n_units for a in army1)
        a2 = sum(a.n_units for a in army2)
        # print()
    # one of this is always 0 so we can return sum of them
    return (a1+a2, (1 if a1 > 0 and not Group.draw else 0))


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def main():
    lines = read_lines("Day_24/inputs/input1")
    if lines is None:
        return 1
    infection = []
    immune = []
    army = immune
    side = 'IMMUNE'
    id = 1
    for line in lines[1:]:
        if len(line) == 0:
            continue
        if line == 'Infection:':
            army = infection
            side = 'INFECTION'
            id = 1
            continue
        (n_units, HP, mods, AP, damage, ini) = re.match(
            r'(\d+) units each with (\d+) hit points(?: \((.*?)\))?'
            r' with an attack that does (\d+) (\w+) damage at initiative (\d+)', line).groups()
        weaknesses = None
        immunes = None
        if mods:
            m = re.search(r'(?<=weak\sto\s)(\w+|,\s)+', mods)
            if m:
                weaknesses = m.group()
            m = re.search(r'(?<=immune\sto\s)(\w+|,\s)+', mods)
            if m:
                immunes = m.group()
        army.append(Group(int(n_units), int(HP), int(AP), int(
            ini), weaknesses, immunes, damage, side, id))
        id += 1

    ###############################################
    # Part One
    result = war(copy.deepcopy(immune), copy.deepcopy(infection))

    print("Part 1: ", result[0])
    ###############################################
    # Part Two
    for boost in itertools.count(1):
        (result, winner) = war(copy.deepcopy(
            immune), copy.deepcopy(infection), boost)
        if winner == 1:
            break
    print("Part 2: ", result)
    return 0


main()
