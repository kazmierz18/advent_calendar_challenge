/*===============*/
/* Day 12        */
/*===============*/
#include <algorithm>
#include <fstream>
#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <vector>

auto split(const std::string &line, const char *delimiter) {
  std::vector<std::string> splitted;
  auto currentIdx = -1;
  do {
    currentIdx++;
    const auto foundIdx = line.find_first_of(delimiter, currentIdx);
    splitted.push_back(line.substr(currentIdx, foundIdx - currentIdx));
    currentIdx = foundIdx;
  } while (currentIdx != std::string::npos);
  return splitted;
}

struct Program {
  Program(int const id) : id(id) {}
  int id;
  int id_checking = -1;
  bool zero;
  bool was_checked;
  std::vector<int> links;
  bool links_to_zero(std::map<int, std::shared_ptr<Program>> &map,
                     int const parent_id) {
    if (was_checked) {
      return zero;
    }

    for (auto l : links) {
      if (parent_id == l) {
        continue;
      }
      if (l == 0) {
        zero = true;
        break;
      }
      if (map[l]->id_checking == id_checking) {
        continue;
      }
      map[l]->id_checking = id_checking;
      if (map[l]->links_to_zero(map, l)) {

        std::cout <<  l << "<-";
        zero = true;
        break;
      }
    }
    was_checked = true;
    return zero;
  }
};

int main(int argc, char **argv) {
  // read input file
  char *name = nullptr;
  if (argc >= 2) {
    name = argv[1];
  } else {
    return 1;
  }
  std::ifstream file(name);
  std::string tmp;
  std::vector<std::string> lines;
  while (std::getline(file, tmp)) {
    // remove new line characters
    if (*(tmp.end()) == '\n') {
      tmp.pop_back();
    }
    lines.push_back(tmp);
  }
  std::map<int, std::shared_ptr<Program>> programs;
  for (auto const &line : lines) {
    auto const s = split(line, " ");
    auto const id = atoi(s[0].c_str());
    auto p = std::shared_ptr<Program>(new Program(id));
    for (auto i = s.begin() + 2; i != s.end(); i++) {
      auto ii = *i;
      ii.erase(std::remove(ii.begin(), ii.end(), ','), ii.end());
      p->links.push_back(atoi(ii.c_str()));
    }
    programs.insert({id, p});
  }
  int count = 0;
  for (auto &p : programs) {
    p.second->id_checking = p.first;
    if (p.second->links_to_zero(programs, p.first)) {
      std::cout << p.first << "\n";
      count++;
    }
  }
  //================================================
  std::cout << "Part One: " << count << "\n";
  //================================================
  std::cout << "Part Two: "
            << "\n";
  return 0;
}
