"""Day 08 problem solver"""


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def main():
    lines = read_lines("Day_08/inputs/input1")
    if lines is None:
        return 1
    digits = map(int, lines[0])
    WIDTH = 25
    HEIGHT = 6
    layers = []
    layer = []
    for d in digits:
        if len(layer) == WIDTH*HEIGHT:
            layers.append(layer)
            layer = []
        layer.append(d)
    layers.append(layer)
    ###############################################
    # Part One
    zeros = [l.count(0) for l in layers]
    idx = zeros.index(min(zeros))
    print("Part 1: "+str(layers[idx].count(1)*layers[idx].count(2)))
    ###############################################
    # Part Two
    image = [2 for x in range(WIDTH*HEIGHT)]
    for layer in layers:
        for y in range(HEIGHT):
            for x in range(WIDTH):
                image[x+WIDTH*y] = layer[x+WIDTH*y] if image[x+WIDTH*y]==2 else image[x+WIDTH*y]
                
    for y in range(HEIGHT):
        for x in range(WIDTH):
            print(' ' if image[x+WIDTH*y] == 0 else "#", end='')
        print()
    return 1


main()
