import os
import time
import matplotlib.pyplot as plt
import numpy as np
import re


def main():
    results_times = []
    problem_names = []
    NUM_OF_DAYS = 25
    total_time = time.time()
    for i in range(NUM_OF_DAYS):
        folder_path = "Day_"+"{:02d}".format(i+1)
        script_name = "python3 "+folder_path+"/solve.py"
        challenge_path = folder_path+"/Challenge.md"
        try:
            with open(challenge_path) as ch_f:
                line = ch_f.readline()
                res = re.search('(?!#)(.*)(?<!-)', line)
                problem_names.append(res.group(0).strip())
        except:
            continue
        try:
            print(problem_names[-1])
            start = time.time()
            os.system(script_name)
            end=time.time()-start
            print('Time '+str(end))
            results_times.append(end)
        except:
            pass
    total_time = time.time()-total_time
    title = 'Total time: '+ str(total_time)
    index = np.arange(len(problem_names))
    plt.bar(index, results_times[:len(problem_names)])
    plt.xlabel('problem')
    plt.ylabel('time in s')
    plt.xticks(index, problem_names, rotation=80)
    plt.tight_layout()
    plt.title(title)
    plt.grid(True)
    plt.show()


main()
