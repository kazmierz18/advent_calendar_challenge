"""Day 11 problem solver"""

import numpy as np
from matplotlib import pyplot as plt


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def main():
    lines = read_lines("Day_11/inputs/input1")
    if lines is None:
        return 1
    serial_ID = int(lines[0])
    ARRAY_SIZE = 300
    fuel_matrix = np.zeros([ARRAY_SIZE, ARRAY_SIZE])
    for x in range(ARRAY_SIZE):
        for y in range(ARRAY_SIZE):
            rackId = (x+1)+10
            fuel_matrix[x][y] = int(
                ((((rackId*(y+1)+serial_ID)*rackId) // 100) % 10) - 5)

    # plot input data
    # plt.imshow(fuel_matrix)
    # plt.show()

    ###############################################
    # Part One

    # Use summed area table algorithm
    I = np.zeros([ARRAY_SIZE, ARRAY_SIZE])
    for x in range(ARRAY_SIZE):
        for y in range(ARRAY_SIZE):
            # I(x,y) = i(x,y) + I(x,y-1) + I(x-1,y) - I(x-1,y-1)
            I[x][y] = fuel_matrix[x][y]+I[x][y-1]+I[x-1][y]-I[x-1][y-1]

    # plot summed area table
    # plt.imshow(I)
    # plt.show()

    px = 0
    py = 0
    max_fuel_lvl = 0
    WINDOW_SIZE = 3
    for x in range(ARRAY_SIZE-WINDOW_SIZE):
        for y in range(ARRAY_SIZE-WINDOW_SIZE):
            #
            # A   B
            #
            # C   D
            #
            # i(x,y) = I(D) + I(A) - I(B) - I(C)
            lvl = I[x+WINDOW_SIZE][y+WINDOW_SIZE]+I[x, y] - \
                I[x+WINDOW_SIZE][y]-I[x][y+WINDOW_SIZE]
            if lvl > max_fuel_lvl:
                max_fuel_lvl = lvl
                px = x+2
                py = y+2

    print("Part 1: ", px, ",", py)
    ###############################################
    # Part Two

    px = 0
    py = 0
    max_fuel_lvl = 0
    best_size = 0
    for size in range(2, ARRAY_SIZE):
        for x in range(ARRAY_SIZE-size):
            for y in range(ARRAY_SIZE-size):
                #
                # A   B
                #
                # C   D
                #
                # i(x,y) = I(D) + I(A) - I(B) - I(C)
                lvl = I[x+size][y+size]+I[x, y]-I[x+size][y]-I[x][y+size]
                if lvl > max_fuel_lvl:
                    max_fuel_lvl = lvl
                    px = x+2
                    py = y+2
                    best_size = size
                    # print(px, ",", py, ",", best_size,", lvl=",lvl)
    print("Part 2: ", px, ",", py, ",", best_size)
    return 0


main()
