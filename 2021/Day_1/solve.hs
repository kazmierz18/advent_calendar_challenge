module Main where

main :: IO ()
main = do
    content <- readFile "Day_1/inputs/input1"
    let input = parseInput content
    print $ part1 input
    print $ part2 input

parseInput :: String -> [Int]
parseInput input = map (read::String->Int) (lines input)

pairs :: [b] -> [(b, b)]
pairs xs = zip xs (tail xs)

triplets :: [b] -> [(b, b, b)]
triplets xs = zip3 xs (tail xs) (tail $ tail xs)

sumThree::[(Int,Int,Int)] ->[Int]
sumThree = map (\(x, y, z) -> x+y+z)

part1 input = length $ filter (uncurry (<)) (pairs input)
part2 input = length $ filter (uncurry (<)) ( pairs $ sumThree $ triplets input)