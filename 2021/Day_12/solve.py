"""Day 12 problem solver"""

from pprint import pprint
from collections import defaultdict, Counter


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None

def pred1(n, path):
    return n.isupper() or n not in path

def pred2(n, path):
    return pred1(n,path) or max(Counter(filter(str.islower, path)).values()) == 1


    return n.isupper() or n not in path

def paths(G, current, path, pred):
    if current == 'end': 
        return [path]

    res = []
    for x in G[current]:
        if pred(x, path):
            res += paths(G, x, path + [x], pred)

    return res

def main():
    lines = read_lines('Day_12/inputs/input1')
    G = defaultdict(lambda: list(), {})
    for line in lines:
        (l, r) = line.strip().split("-")
        if l != 'end' and r != 'start': G[l].append(r)
        if r != 'end' and l != 'start': G[r].append(l)
    routes =paths(G, "start", [], pred1)
    ret= len(routes)

    ###############################################
    # Part One
    print(f"Part 1: {ret}")
    ###############################################
    # Part Two
    routes =paths(G, "start", [], pred2)
    ret= len(routes)
    print(f"Part 2: {ret}")
    return 0


main()
