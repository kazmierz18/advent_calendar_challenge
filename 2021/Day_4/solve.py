"""Day 4 problem solver"""
import pprint
import re

def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None

class BingoBoard():
    def __init__(self, lines):
        self.board = []
        for line in lines:
            ints = re.split(r'\s+', line.strip())
            self.board.append([int(x) for x in ints])

    def __check_rows(self):
        for row in self.board:
            if sum(row)==-len(row):
                return True
        return False
    
    def __check_columns(self):
        for y in range(len(self.board)):
            sum = 0
            for x in range(len(self.board[0])):
                sum += self.board[x][y]
            if sum == - len(self.board):
                return True
        return False


    def is_wining(self, num):
        for y in range(len(self.board)):
            for x in range(len(self.board[0])):
                if self.board[y][x]==num:
                    self.board[y][x] = -1
        
        return self.__check_columns() or self.__check_rows()

    def sum(self):
        s = 0
        for y in range(len(self.board)):
            for x in range(len(self.board[0])):
                if self.board[y][x] > 0:
                    s += self.board[y][x]
        return s

    def print_board(self):
        pprint.pprint(self.board)


def main():
    lines = read_lines("Day_4/inputs/input1")
    if lines is None:
        return 1
    ###############################################
    # Part One
    rand_input = [int(x) for x in lines[0].split(',')]
    lines = lines[2:]
    SIZE = 5
    boards = []
    for x in range(0, len(lines), SIZE+1):
        bb = BingoBoard(lines[x:x+SIZE])
        boards.append(bb)

    def wining_bingo(num):
        wbs = []
        for bb in boards:
            if bb.is_wining(num):
               wbs.append(bb)
        return wbs

    ret = 0
    for num in rand_input:
        wbs = wining_bingo(num)
        if wbs:
            ret = num * wbs[0].sum()
            break


    print("Part 1: %d"%ret)
    ###############################################
    # Part Two
    for i, num in enumerate(rand_input):
        wbs = wining_bingo(num)
        if wbs:
            for wb in wbs:
                boards.remove(wb)
            if len(boards)==0:
                break
    wb.print_board()
    ret = rand_input[i] * wb.sum()
    print("Part 2: %d"%ret)
    return 0


main()
