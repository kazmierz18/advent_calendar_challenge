"""Day 11 problem solver"""

from intcode import intcode_cpu, preprocess
from queue import Queue
from threading import Thread


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


class CPU_thread(Thread):
    def __init__(self, cpu, id):
        Thread.__init__(self)
        self.id = id
        self.cpu = cpu

    def run(self):
        self.cpu.run()


UP, LEFT, RIGHT, DOWN = 0, 1, 2, 3


def get_dir(current, turn):
    if current == UP:
        if turn == 0:
            return LEFT
        if turn == 1:
            return RIGHT
    if current == LEFT:
        if turn == 0:
            return DOWN
        if turn == 1:
            return UP
    if current == DOWN:
        if turn == 0:
            return RIGHT
        if turn == 1:
            return LEFT
    if current == RIGHT:
        if turn == 0:
            return UP
        if turn == 1:
            return DOWN


def get_pos(current, dir):
    if dir == UP:
        return (current[0], current[1]-1)
    if dir == DOWN:
        return (current[0], current[1]+1)
    if dir == RIGHT:
        return (current[0]+1, current[1])
    if dir == LEFT:
        return (current[0]-1, current[1])


def main():
    lines = read_lines("Day_11/inputs/input1")
    if lines is None:
        return 1
    program = preprocess(lines)
    for _ in range(500):
        program.append(0)
    ###############################################
    # Part One
    cpu = intcode_cpu()
    cpu.load_program(program)
    cpu.input = Queue(1)
    cpu.output = Queue(2)
    th = CPU_thread(cpu, 0)
    HEIGHT = 150
    WIDTH = 200
    hull = [[(0, 0) for _ in range(WIDTH)] for _ in range(HEIGHT)]
    th.start()
    position = (WIDTH//2, HEIGHT//2)
    hull[position[1]][position[0]] = (1, 0)
    dir = UP
    while th.is_alive():
        cpu.input.put(hull[position[1]][position[0]][0])
        try:
            color = cpu.output.get(timeout=1)
        except:
            break
        hull[position[1]][position[0]] = (
            color, hull[position[1]][position[0]][1] + 1)
        turn = cpu.output.get()
        dir = get_dir(dir, turn)
        position = get_pos(position, dir)

    th.join()
    cnt = 0
    for line in hull:
        for plate in line:
            print("#" if plate[0] == 1 else ".", end="")
            if plate[1] > 0:
                cnt += 1
        print()
    print("Part 1: " + str(cnt))
    ###############################################
    # Part Two
    print("Part 2: ")
    return 0


main()
