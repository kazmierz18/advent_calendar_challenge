# AOC
My solutions written in python for advent of code challenge

Link to page: [adventofcode](https://adventofcode.com/2018)

### Explanation

In this challenge I am trying to learn more Python.
My goal is to keep code mostly readable but still quite fast (I am not trying to optimize LOC and do weird unreadable magic construction).
Maybe later I will do a C++ version with fastest possible code.
I am also not trying to participate in the leader board, mainly because of work.

#### Folder hierarchy:
```
├── Day_n             -> folder for each day
│   ├── Challenge.md  -> copy of problem scenario in case the page will go down in the future
│   ├── inputs
│   │   ├── input1    -> problem to solve
│   │   └── input2    -> test scenarios
│   └── solve.py      -> main solving function
```
This hierarchy is constructed via simple script *create_day.sh* in the root folder