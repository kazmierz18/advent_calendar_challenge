"""Day 10 problem solver"""

import random
import matplotlib.pyplot as plt


class Star:
    max_y = 0
    max_x = 0
    min_y = 2 << 32
    min_x = 2 << 32

    def __init__(self, x, y, dx, dy):
        self.x = x
        self.y = y
        self.dx = dx
        self.dy = dy
        self.size_of_constellation()

    def size_of_constellation(self):
        "Calculate current size of constellation with this star"
        Star.max_x = max(Star.max_x, self.x)
        Star.max_y = max(Star.max_y, self.y)
        Star.min_x = min(Star.min_x, self.x)
        Star.min_y = min(Star.min_y, self.y)

    def fly(self, t):
        "Falling star, move star by a t amount of time"
        self.x += self.dx*t
        self.y += self.dy*t

    def fly_on_sky(self, sky):
        "Falling star, move star and draw it on char table sky"
        self.draw(sky, '.')
        self.fly(1)
        self.draw(sky, '#')

    def draw(self, sky, char):
        "Draw stars on char table sky"
        if sky and self.y >= 0 and self.x >= 0 and self.y <= Star.max_y and self.x <= Star.max_x:
            sky[self.y][self.x] = char

    def rebase(self):
        "Move whole constelation to 1st quarter of cartesian coordinate system"
        self.x -= Star.min_x
        self.y -= Star.min_y

    @staticmethod
    def reset_constellation():
        Star.max_x = 0
        Star.max_y = 0
        Star.min_x = 2 << 32
        Star.min_y = 2 << 32

    @staticmethod
    def constelation_size():
        "Return size of current constellation (area)"
        return (Star.max_x - Star.min_x)*(Star.max_y - Star.min_y)


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def main():
    lines = read_lines("Day_10/inputs/input1")
    if lines is None:
        return 1

    constellation = []
    # parse input and create constelation
    for line in lines:
        idx = line.find('>')
        position = line[10:idx]
        parts = position.split(',')
        x = int(parts[0])
        y = int(parts[1])
        idx = line.find('<', idx)+1
        velocity = line[idx:-1]
        parts = velocity.split(',')
        dx = int(parts[0])
        dy = int(parts[1])
        constellation.append(Star(x, y, dx, dy))

    ###############################################
    # Part One
    # some linear algebra to skip a lot of simulation
    # y=ax +b
    # y=cx +d
    # ax+b=xc+d
    # ax-cx=d-b
    # x(a-c)=d-b
    # x=(d-b)/(a-c)
    # assumin text is close to cross of linear function made by moving stars,
    # we pick one randomly and calculate point of crossing which produces starting t
    star = random.choice(constellation)
    a,b,c,d=0,0,0,0
    if star.dx!=0:
        a = star.dy/float(star.dx)
    if a*star.x !=0:
        b = star.y/float(a*star.x)
    t = []
    for star2 in constellation:
        star2 = random.choice(constellation)
        if star2.dx!=0:
            c = star2.dy/float(star2.dx)
        if c*star2.x != 0:
            d = star2.y/float(c*star2.x)
        if (a-c) != 0:
            x = (d-b)/(a-c)
            t.append(int(abs(star2.x-x)))
    t.sort()
    print(t[0], ' ', t[-1])
    time_passed = t[0]  # pick lowest and start simulation from here
    print(time_passed)
    print()

    # skip simulation by time calculated from equation
    for star in constellation:
        star.fly(time_passed)
        star.size_of_constellation()

    # now start the simulation until the area is still droping
    # constellation with smallest area is the one with text
    prev_area = 2 << 32
    for i in range(t[-1]-t[0]):
        time_passed += 1
        Star.reset_constellation()
        for star in constellation:
            star.fly(1)
            star.size_of_constellation()
        area = Star.constelation_size()
        if prev_area < area:
            break
        prev_area = area

    # we need to reverse last step 
    for star in constellation:
        star.fly(-1)
    xs = [star.x for star in constellation]
    ys = [-star.y for star in constellation]


    for star in constellation:
        star.rebase()
    Star.reset_constellation()
    for star in constellation:
        star.size_of_constellation()
    sky=[['.' for x in range(Star.max_x+1)] for y in range(Star.max_y+1)]
    for star in constellation:
        star.draw(sky,'#')
    
    for skyline in sky:
        print("".join(skyline))
    # using matplotlib
    # plt.figure(figsize=(8,2))
    # plt.plot(xs, ys, linestyle="", marker="o")
    
    # plt.show()

    print("Part 1: On the plot")
    # ###############################################
    # Part Two
    print("Part 2: ", time_passed)
    return 0


main()
