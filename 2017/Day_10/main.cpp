/*===============*/
/* Day 10        */
/*===============*/
#include <fstream>
#include <iostream>
#include <string>
#include <vector>


int main(int argc, char **argv){
  // read input file
  char *name = nullptr;
  if (argc >= 2) {
    name = argv[1];
  }else{
    return 1;
  }
  std::ifstream file(name);
  std::string tmp;
  std::vector<std::string> lines;
  while (std::getline(file, tmp)) {
    // remove new line characters
    if (*(tmp.end()) == '\n') {
       tmp.pop_back();
    }
    lines.push_back(tmp);
  }
  std::cout << lines[0] << "\n";
  //================================================
  std::cout << "Part One: " << "\n";
  //================================================
  std::cout << "Part Two: " << "\n";
  return 0;
}
