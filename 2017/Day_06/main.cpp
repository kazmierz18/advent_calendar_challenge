/*===============*/
/* Day 06        */
/*===============*/
#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

auto split(const std::string &line, const char *delimiter) {
  std::vector<std::string> splitted;
  auto currentIdx = -1;
  do {
    currentIdx++;
    const auto foundIdx = line.find_first_of(delimiter, currentIdx);
    splitted.push_back(line.substr(currentIdx, foundIdx - currentIdx));
    currentIdx = foundIdx;
  } while (currentIdx != std::string::npos);
  return splitted;
}

void redistribute(std::vector<int> &numbers) {
  auto item = std::max_element(numbers.begin(), numbers.end());
  auto value = *item;
  *item = 0;
  while (value != 0) {
    value--;
    item++;
    if (item == numbers.end()) {
      item = numbers.begin();
    }
    (*item) += 1;
  }
}

int main(int argc, char **argv) {
  // read input file
  char *name = nullptr;
  if (argc >= 2) {
    name = argv[1];
  } else {
    return 1;
  }
  std::ifstream file(name);
  std::string tmp;
  std::vector<std::string> lines;
  while (std::getline(file, tmp)) {
    // remove new line characters
    if (*(tmp.end()) == '\n') {
      tmp.pop_back();
    }
    lines.push_back(tmp);
  }
  //================================================
  auto splitted = split(lines[0], "\t ");
  std::vector<int> numbers;
  numbers.reserve(splitted.size());
  for (auto const &item : splitted) {
    numbers.push_back(atoi(item.c_str()));
  }
  std::vector<decltype(numbers)> history;
  history.push_back(numbers);
  auto counter = 0;
  while (true) {
    redistribute(numbers);
    counter++;
    if (std::find(history.begin(), history.end(), numbers) != history.end()) {
      break;
    }
    history.push_back(numbers);
  }
  std::cout << "Part One: " << counter << "\n";

  //================================================
  counter = 1;
  const auto compare = history.end() - 1;
  while (true) {
    redistribute(numbers);
    counter++;
    if (numbers == *compare) {
      break;
    }
  }
  std::cout << "Part Two: " << counter << "\n";
  return 0;
}
