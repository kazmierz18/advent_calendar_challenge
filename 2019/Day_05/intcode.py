from copy import deepcopy


def preprocess(lines):
    opcodes = lines[0].split(',')
    opcodes = [int(oc) for oc in opcodes]
    return opcodes


POSITION_MODE, IMMEDIATE_MODE, RELATIVE_MODE = 0, 1, 2


def b_open(mode):
    d = {IMMEDIATE_MODE: "", POSITION_MODE: "[", RELATIVE_MODE: "{"}
    return d[mode]


def b_close(mode):
    d = {IMMEDIATE_MODE: "", POSITION_MODE: "]", RELATIVE_MODE: "}"}
    return d[mode]


class intcode_cpu:
    def __init__(self):
        self.memory = []
        self.PC = 0
        self.output = None
        self.input = None
        self.rbx = 0
        self.output_stream = ""
        self.instructions = {
            1: (self.add, 'ADD'),
            2: (self.mul, 'MUL'),
            99: (self.stop, 'STOP'),
            3: (self.ld, 'LD'),
            4: (self.out, 'OUT'),
            5: (self.jmp_t, 'JMP_T'),
            6: (self.jmp_f, 'JMP_F'),
            7: (self.lt, 'LT'),
            8: (self.eq, 'EQ'),
            9: (self.rb, 'RB')
        }

    def rb(self, mode):
        self.rbx += self.get_value(self.PC+1, mode[0])
        self.PC += 2
        return True

    def jmp_f(self, modes):
        if not self.get_value(self.PC+1, modes[0]):
            self.PC = self.get_value(self.PC+2, modes[1])
        else:
            self.PC += 3
        return True

    def jmp_t(self, modes):
        if self.get_value(self.PC+1, modes[0]):
            self.PC = self.get_value(self.PC+2, modes[1])
        else:
            self.PC += 3
        return True

    def lt(self, modes):
        self.set_value(modes[2], 1 if self.get_value(
            self.PC+1, modes[0]) < self.get_value(self.PC+2, modes[1]) else 0, self.PC+3)
        self.PC += 4
        return True

    def eq(self, modes):
        self.set_value(modes[2], 1 if self.get_value(
            self.PC+1, modes[0]) == self.get_value(self.PC+2, modes[1]) else 0, self.PC+3)
        self.PC += 4
        return True

    def ld(self, modes):
        if self.input:
            self.set_value(modes[0], self.input.get(), self.PC+1)
        else:
            self.set_value(modes[0], int(input()), self.PC+1)
        self.PC += 2
        return True

    def out(self, modes):
        # print(self.get_value(self.PC+1, modes[0]))
        if self.output:
            self.output.put(self.get_value(self.PC+1, modes[0]))
        self.PC += 2
        return True

    def stop(self, modes):
        return False

    def print_debug(self, name, modes, cmd):
        print(str(self.PC)+" "+name, end=" ")
        print(b_open(modes[0]) + str(self.memory[self.PC+1]
                                     ) + b_close(modes[0]), end=" ")
        if cmd == 1 or cmd == 2 or cmd == 7 or cmd == 8:
            print(b_open(modes[1]) + str(self.memory[self.PC+2]
                                         ) + b_close(modes[1]), end=" ")
            print("-> [" + str(self.memory[self.PC+3]
                               ) + "]", end=" ")
        print()

    def add(self, modes):
        self.set_value(modes[2], self.get_value(
            self.PC+1, modes[0]) + self.get_value(self.PC+2, modes[1]), self.PC+3)
        self.PC += 4
        return True

    def mul(self, modes):
        self.set_value(modes[2], self.get_value(
            self.PC+1, modes[0]) * self.get_value(self.PC+2, modes[1]), self.PC+3)
        self.PC += 4
        return True

    def execute(self):
        command = self.memory[self.PC] % 100
        o = self.memory[self.PC]
        modes = [o % 1000//100, o %
                 10000//1000, o % 100000//10000]
        # self.print_debug(self.instructions[command][1], modes, command)
        return self.instructions[command][0](modes)

    def run(self):
        while self.execute():
            pass

    def load_program(self, program):
        self.memory = deepcopy(program)

    def set_value(self, mode, value, pc):
        if mode == RELATIVE_MODE:
            self.memory[self.rbx + self.memory[pc]] = value
        else:
            self.memory[self.memory[pc]] = value

    def get_value(self, pc, mode):
        if mode == POSITION_MODE:
            return self.memory[self.memory[pc]]
        elif mode == IMMEDIATE_MODE:
            return self.memory[pc]
        elif RELATIVE_MODE:
            return self.memory[self.rbx+self.memory[pc]]
        else:
            raise Exception("Unknown mode")
