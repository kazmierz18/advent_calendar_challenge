"""Day 3 problem solver"""

from functools import reduce

def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def match_bit(lines, position, s):
    tmp = 0
    for line in lines:
        if line[position] == "1":
            tmp += s
        else:
            tmp += -s
    if tmp == 0:
        tmp = s
    if tmp > 0:
        return [x for x in lines if x[position] == "1"]
    else:
        return [x for x in lines if x[position] == "0"]


def main():
    lines = read_lines("Day_3/inputs/input1")
    if lines is None:
        return 1
    ###############################################
    # Part One
    transposed = [list(i) for i in zip(*lines)]
    gamma = 0
    epsilon = 0
    def parse(line):
        return "1" if sum(1 if x =="1" else 0 for x in line)*2>=len(line) else "0"
    epsilon_str = "".join((map(parse, transposed)))
    gamma_str ="".join(["0" if x =="1" else "1" for x in epsilon_str])

    epsilon = int(epsilon_str, 2)
    gamma = int(gamma_str, 2)

    ret = epsilon * gamma
    print(f"Part 1: {ret}")
    ###############################################
    # Part Two
    lines_copy = lines
    pos = 0
    while len(lines_copy) > 0:
        transposed = [list(i) for i in zip(*lines)]
        crit = parse(transposed[pos])
        lines_copy = [x for x in lines_copy if x[pos] == crit]
        pos = (pos+1) % len(lines[0])

    out = lines
    while len(lines) > 1:
        out = match_bit(out, pos, 1)
        pos = (pos+1) % len(lines[0])
    oxygen_rating = int(out[0], base=2)
    # 2783
    out = lines
    pos = 0
    while len(out) > 1:
        out = match_bit(out, pos, - 1)
        pos = (pos+1) % len(lines[0])
    co2_rating = int(out[0], base=2)
    ret = oxygen_rating*co2_rating
    print(f"Part 2: {ret}")
    return 0


main()
