/*===============*/
/* Day 05        */
/*===============*/
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

void print_program(const std::vector<int> &program) {
  for (const auto instruction : program) {
    std::cout << instruction << " ";
  }
  std::cout << "\n";
}

struct Behaviour {
  virtual int get(const int val) const { return val; }
};
struct Normal : public Behaviour {
  int get(const int val) const override { return val + 1; }
};
struct Advanced : public Behaviour {
  int get(const int val) const override { return val >= 3 ? val - 1 : val + 1; }
};

template <class Behaviour> struct CPU {
  explicit CPU(Behaviour b) {}
  unsigned PC = 0;
  unsigned stepCnt = 0;
  Behaviour b;
  CPU() = default;
  bool execute(std::vector<int> &program) {
    if (PC >= program.size()) {
      return false;
    }
    const auto tmp = PC;
    PC += program[PC];
    program[tmp] = b.get(program[tmp]);
    stepCnt++;
    return true;
  }
};

int main(int argc, char **argv) {
  // read input file
  char *name = nullptr;
  if (argc >= 2) {
    name = argv[1];
  } else {
    return 1;
  }
  std::ifstream file(name);
  std::string tmp;
  std::vector<std::string> lines;
  while (std::getline(file, tmp)) {
    // remove new line characters
    if (*(tmp.end()) == '\n') {
      tmp.pop_back();
    }
    lines.push_back(tmp);
  }
  std::vector<int> program;
  program.reserve(lines.size());
  for (auto const &line : lines) {
    program.push_back(atoi(line.c_str()));
  }
  //================================================
  decltype(program) origin;
  origin.reserve(program.size());
  std::copy(program.begin(), program.end(), std::back_inserter(origin));
  CPU<Normal> cpu;
  while (cpu.execute(program))
    ;
  // print_program(program);

  std::cout << "Part One: " << cpu.stepCnt << "\n";
  //================================================
  CPU<Advanced> cpu2;
  while (cpu2.execute(origin))
    ;
  std::cout << "Part Two: " << cpu2.stepCnt << "\n";
  return 0;
}
