"""Day 1 problem solver"""

from itertools import tee


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def pairwise(iterable):
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)


def main():
    lines = read_lines("Day_1/inputs/input1")
    if lines is None:
        return 1
    ###############################################
    # Part One
    i_levels = list(map(int, lines))
    ret = len(list(filter(lambda x: x[0] < x[1],pairwise(i_levels))))
    print(f"Part 1: {ret}")
    ###############################################
    # Part Two
    SUM_LEN = 3
    i_levels_SUM_LEN = [(sum(i_levels[i:i+SUM_LEN]))
                        for i in range(len(i_levels)-SUM_LEN+1)]
    ret = len(list(filter(lambda x: x[0] < x[1],pairwise(i_levels_SUM_LEN))))
    print(f"Part 2: {ret}")
    return 0


main()
