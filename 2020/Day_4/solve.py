"""Day 4 problem solver"""


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


class Passport:
    def __init__(self):
        # self.byr=None
        # self.iyr=None
        # self.eyr=None
        # self.hgt=None
        # self.hcl=None
        # self.ecl=None
        # self.pid=None
        # self.cid=None
        self.data = {}

    def append(self, d):
        fields = d.split(' ')
        for f in fields:
            f = f.split(':')
            self.data[f[0]] = f[1]

    def valid(self):
        cmp = [
            'byr',
            'iyr',
            'eyr',
            'hgt',
            'hcl',
            'ecl',
            'pid',
            # 'cid',
        ]
        for c in cmp:
            if c not in self.data.keys():
                return False
        return True

    def valid2(self):
        if not self.valid():
            return False

        x = int(self.data['byr'])
        if x < 1920 or x > 2002:
            return False

        x = int(self.data['iyr'])
        if x < 2010 or x > 2020:
            return False

        x = int(self.data['eyr'])
        if x < 2020 or x > 2030:
            return False

        if not self.validHgt(self.data['hgt']):
            return False
        if not self.validHairColor(self.data['hcl']):
            return False
        if not self.validEyeColor(self.data['ecl']):
            return False
        if not self.validPID(self.data['pid']):
            return False
        return True

    def validHgt(self, hgt):
        if len(hgt) < 3:
            return False
        if 'in' == hgt[-2:]:
            x = int(hgt[:-2])
            return x >= 59 and x <= 76
        elif 'cm' in hgt[-2:]:
            x = int(hgt[:-2])
            return x >= 150 and x <= 193
        return False

    def validEyeColor(self, ecl):
        cl = ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']

        for c in cl:
            if c == ecl:
                return True
        return False

    def validHairColor(self, hcl):
        if hcl[0] == '#':
            hcl = hcl[1:]
        else:
            return False
        if len(hcl) != 6:
            return False
        try:
            int(hcl, 16)
            return True
        except:
            pass
        return False

    def validPID(self, pid):
        if len(pid) != 9:
            return False
        try:
            int(pid)
            return True
        except:
            pass
        return False

    def __str__(self):
        return '%s %s' % (str(self.data['pid']), str(self.data['eyr']))


def main():
    lines = read_lines("Day_4/inputs/input1")
    if lines is None:
        return 1
    ###############################################
    # Part One
    print("Part 1: ")
    passports = [Passport()]
    for line in lines:
        if len(line) == 0:
            passports.append(Passport())
        else:
            passports[-1].append(line)

    cnt = 0
    for p in passports:
        if p.valid():
            cnt += 1

    print(cnt)
    ###############################################
    # Part Two
    print("Part 2: ")
    cnt = 0
    for p in passports:
        if p.valid2():
            cnt += 1

    print(cnt)
    return 0


main()
