/*===============*/
/* Day 01        */
/*===============*/
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

auto char2num(const char c) { return c - '0'; }

int main(int argc, char **argv) {
  // read input file
  char *name = nullptr;
  if (argc >= 2) {
    name = argv[1];
  }else{
    return 1;
  }
  std::ifstream file(name);
  std::string tmp;
  std::vector<std::string> lines;
  while (std::getline(file, tmp)) {
    // remove new line characters
    if (*(tmp.end()) == '\n') {
      tmp.pop_back();
    }
    lines.push_back(tmp);
  }
  std::cout << lines[0] << "\n";
  //================================================
  auto begin = lines[0].begin();
  auto current = begin;
  auto checked = current + 1;
  auto end = lines[0].end();
  auto sum = 0L;
  while (current != end) {
    if(*current == *checked) {
      sum += char2num(*current);
    }
    current++;
    checked++;
    if(checked==end){
        checked=begin;
    }
  }
  std::cout << "Part One: " << sum << "\n";
  //================================================
  current = lines[0].begin();
  checked = current + lines[0].size() / 2;
  sum = 0L;
  while (current != end) {
    if(*current == *checked){
      sum += char2num(*current);
    }
    current++;
    checked++;
    if(checked==end){
        checked=begin;
    }
  }
  std::cout << "Part Two: " << sum << "\n";
  return 0;
}
