"""Day 09 problem solver"""
from intcode import intcode_cpu, preprocess

def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def main():
    lines = read_lines("Day_09/inputs/input1")
    if lines is None:
        return 1
    program=preprocess(lines)
    for _ in range(3000):
        program.append(0)
    ###############################################
    # Part One
    cpu=intcode_cpu()
    cpu.load_program(program)
    cpu.run()
    print("Part 1: ")
    ###############################################
    # Part Two
    cpu=intcode_cpu()
    cpu.load_program(program)
    cpu.run()
    print("Part 2: ")
    return 0


main()
