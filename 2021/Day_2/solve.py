"""Day 2 problem solver"""
import re

from functools import reduce
from collections import namedtuple


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


Point = namedtuple('Point', ['x', 'y'])


def parse(line):
    m = re.findall(r'(\w+)\s+(\d+)', line)
    if m:
        if m[0][0] == 'forward':
            return Point(int(m[0][1]), 0)
        elif m[0][0] == 'up':
            return Point(0, -int(m[0][1]))
        elif m[0][0] == 'down':
            return Point(0, +int(m[0][1]))


def main():
    lines = read_lines("Day_2/inputs/input1")
    if lines is None:
        return 1
    ###############################################
    # Part One
    points = map(parse, lines)
    def sum_points(l, r): return Point(l.x+r.x, l.y+r.y)
    position = reduce(sum_points, points, Point(0, 0))
    ret = position.x*position.y
    print(f"Part 1: {ret}")
    ###############################################
    # Part Two
    x, y, aim = 0, 0, 0
    for line in lines:
        m = re.findall(r'(\w+)\s+(\d+)', line)
        if m:
            if m[0][0] == 'forward':
                x += int(m[0][1])
                y += aim*(int(m[0][1]))
            elif m[0][0] == 'up':
                aim -= int(m[0][1])
            elif m[0][0] == 'down':
                aim += int(m[0][1])

    print("Part 1: ", str(x*y))
    return 0


main()
