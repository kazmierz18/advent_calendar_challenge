"""Day 1 problem solver"""

import itertools


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def main():
    lines = read_lines("Day_1/inputs/input1")
    if lines is None:
        return 1
    ###############################################
    # Part One
    numbers = []
    for line in lines:
        numbers.append(int(line))

    pairs = itertools.combinations(numbers, 2)
    ret = 0
    for x, y in pairs:
        if x+y == 2020:
            ret = x*y
            break
    print("Part 1: ")
    print(ret)

    ###############################################
    # Part Two
    triples = itertools.combinations(numbers, 3)
    for x, y, z in triples:
        if x+y+z == 2020:
            ret = x*y*z
            break
    print("Part 2: ")
    print(ret)
    return 0


main()
