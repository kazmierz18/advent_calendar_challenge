"""Day 02 problem solver"""
import sys
from intcode import intcode_cpu, preprocess

def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None

def find(program):
    noun=0
    verb=0
    for noun in range(100):
        for verb in range(100):
            program[1] = noun
            program[2] = verb
            cpu=intcode_cpu()
            cpu.load_program(program)
            try:
                cpu.run()
                if cpu.memory[0]==19690720:
                    return (noun, verb)
            except:
                pass
    raise Exception('Not found')


def main():
    lines = read_lines("Day_02/inputs/input1")
    if lines is None:
        return 1
    program=preprocess(lines)
    # patch
    program[1] = 12
    program[2] = 2
    cpu = intcode_cpu()
    cpu.load_program(program)
    ###############################################
    # Part One
    cpu.run()
    print("Part 1: " + str(cpu.memory[0]))
    ###############################################
    # Part Two
    noun,verb=find(program)
    print("Part 2: " + str(100*noun+verb))
    return 0


main()
