"""Day 6 problem solver"""
import matplotlib.pyplot as plt


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def breed(count, inp):
    days = [0 for _ in range(7)]
    new_breed = [0, 0]
    for i in inp:
        days[i] += 1
    for d in range(count):
        tmp = new_breed[0]
        new_breed[0] = new_breed[1]
        new_breed[1] = +days[d % 7]
        days[d % 7] += tmp
    return sum(days)+sum(new_breed)


def main():
    lines = read_lines("Day_6/inputs/input1")
    if lines is None:
        return 1
    ###############################################
    # Part One
    inp = [int(x) for x in lines[0].split(',')]
    flen = breed(80, inp)
    print("Part 1: %d" % flen)
    ###############################################
    # Part Two

    flen = breed(256, inp)
    print("Part 2: %d" % flen)
    return 0


main()
