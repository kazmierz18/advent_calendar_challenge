"""Day 16 problem solver"""


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


PATTERN = [0, 1, 0, -1]


def main():
    lines = read_lines("Day_16/inputs/input1")
    if lines is None:
        return 1
    numbers = [int(x) for x in lines[0]]
    # ###############################################
    # # Part One
    patterns = []
    for i in range(1, len(numbers)+1):
        pattern = []
        for p in PATTERN:
            pattern += [p]*i
        patterns.append(pattern)
    for idx, _ in enumerate(patterns):
        tmp = len(numbers)//len(patterns[idx])+1
        if tmp > 1:
            patterns[idx] = patterns[idx]*tmp
    for idx, _ in enumerate(patterns):
        patterns[idx] = patterns[idx][1:]
    for _ in range(100):
        output = [0 for _ in range(len(numbers))]
        for i, p in enumerate(patterns):
            for idx, _ in enumerate(numbers):
                output[i] += p[idx]*numbers[idx]
            for idx, o in enumerate(output):
                output[idx] = o % 10
        numbers = [x for x in output]
    txt = [str(x) for x in numbers[:8]]
    print("Part 1: " + "".join(txt))
    ###############################################
    # Part Two
    offset = int(lines[0][:7])
    real_input = [int(x) for x in lines[0]] * 10000
    numbers = real_input[offset-1:]
    for _ in range(100):
        last_sum = 0
        for x in range(len(numbers)-1, 0, -1):
            last_sum += numbers[x]
            numbers[x] = last_sum % 10
    txt = [str(x) for x in numbers[1:9]]
    print("Part 2: "+"".join(txt))
    return 0


main()
