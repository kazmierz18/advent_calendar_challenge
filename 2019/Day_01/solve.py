"""Day 01 problem solver"""


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def calculate_fuel(mass):
    fuel = mass//3-2
    if fuel > 0:
        return fuel+calculate_fuel(fuel)
    else:
        return 0


def main():
    lines = read_lines("Day_01/inputs/input1")
    if lines is None:
        return 1
    values = list(map(int, lines))
    ret = sum(map(lambda v: v//3-2, values))
    ###############################################
    # Part One
    print(f"Part 1: {ret}")
    ###############################################
    # Part Two
    ret = sum(map(calculate_fuel, values))
    print(f"Part 2: {ret}")
    return 0


main()
