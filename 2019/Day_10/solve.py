"""Day 10 problem solver"""
import math
from collections import defaultdict


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


class Asteroid:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.visible = 0
        # polar
        self.th = 0
        self.r = 0

    def polar(self, zero):
        x = self.x - zero.x
        y = self.y - zero.y
        self.th = math.atan2(y, x)
        self.r = math.sqrt(x**2+y**2)


def distance(a, b):
    return math.sqrt(abs(a.x-b.x)**2+(abs(a.y-b.y)**2))


def main():
    lines = read_lines("Day_10/inputs/input1")
    if lines is None:
        return 1
    asteroids = []
    for y, line in enumerate(lines):
        for x, c in enumerate(line):
            if c == '#':
                asteroids.append(Asteroid(x, y))
    for a in asteroids:
        for b in asteroids:
            b.polar(a)
        uniq = defaultdict(int)
        for b in asteroids:
            if a == b:
                continue
            uniq[b.th] = b.r

        a.visible = len(uniq)
    best = max(asteroids, key=lambda a: a.visible)

    ###############################################
    # Part One
    print("Part 1: " + str(best.visible))
    ###############################################
    # Part Two
    bet = asteroids[0]
    for a in asteroids:
        a.polar(best)
    asteroids.remove(best)
    laser = defaultdict(list)
    HALF_PI = math.pi/2
    for a in asteroids:
        if a.th >= -HALF_PI:
            a.th += HALF_PI
        else:
            a.th += 2*math.pi*HALF_PI
    asteroids.sort(key=lambda a: (a.th, a.r))
    for a in asteroids:
        laser[a.th].append(a)
    x = 0
    BET_ID = 200
    while x < BET_ID:
        for beam in laser:
            bet = laser[beam][0]
            laser[beam].remove(bet)
            x += 1
            if x >= BET_ID:
                break
        delete = [key for key in laser if len(laser[key]) == 0]
        for key in delete:
            del laser[key]
    print("Part 2: "+str(bet.x*100+bet.y))
    return 0


main()
