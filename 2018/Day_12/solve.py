"""Day 12 problem solver"""


class Transition:
    def __init__(self, before, after):
        self.before = before
        self.after = after

    def match(self, input):
        return self.before == input


def evolve(times, origin, transitions):
    origin = '...'+origin+'....'
    current_zero_pos = 3
    prev_sum = 0
    prev_diff = 0
    diff = 0
    for step in range(times):
        current_zero_pos += 1
        # print(step+1, origin)
        generation = '...'
        for i in range(len(origin)-2):
            to_match = origin[i:i+5]
            found = False
            for t in transitions:
                if t.match(to_match):
                    generation += t.after
                    found = True
                    break
            if not found:
                generation += '.'

        sum = 0
        for idx, l in enumerate(generation):
            if l == '#':
                sum += idx-current_zero_pos

        diff = sum-prev_sum
        # print(step+1, sum, diff)

        if origin[current_zero_pos:-1] in generation and diff == prev_diff:
            sum += diff*(times-step-1)
            return sum
        origin = generation
        if origin[-1] != '.':
            origin += '....'
        elif origin[-2] != '.':
            origin += '...'
        elif origin[-3] != '.':
            origin += '.'

        prev_diff = diff
        prev_sum = sum

    return sum


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def main():
    lines = read_lines("Day_12/inputs/input1")
    if lines is None:
        return 1
    origin = lines[0].split(' ')[2]
    transitions = []
    for line in lines[2:]:
        parts = line.split(' ')
        # skip '.' for faster matching
        if parts[2] == '#':
            transitions.append(Transition(parts[0], parts[2]))

    ###############################################
    # Part One

    print("Part 1: ", evolve(20, origin, transitions))
    ###############################################
    # Part Two
    print("Part 2: ", evolve(50000000000, origin, transitions))
    return 0


main()
