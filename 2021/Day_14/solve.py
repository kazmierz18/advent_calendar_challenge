"""Day 14 problem solver"""


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def main():
    lines = read_lines("Day_14/inputs/input2")
    if lines is None:
        return 1
    ###############################################
    # Part One
    ret = len(lines)
    print(f"Part 1: {ret}")
    ###############################################
    # Part Two
    print(f"Part 2: {ret}")
    return 0


main()
