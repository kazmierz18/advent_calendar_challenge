"""Day 16 problem solver"""

import re


class ALU:
    registers = [0]*4

    def __init__(self):
        ALU.op_codes = {0: ALU.addr,
                        1: ALU.addi,
                        2: ALU.mulr,
                        3: ALU.muli,
                        4: ALU.banr,
                        5: ALU.bani,
                        6: ALU.borr,
                        7: ALU.bori,
                        8: ALU.setr,
                        9: ALU.seti,
                        10: ALU.gtir,
                        11: ALU.gtri,
                        12: ALU.gtrr,
                        13: ALU.eqir,
                        14: ALU.eqri,
                        15: ALU.eqrr}

    @staticmethod
    def set_registers(A, B, C, D):
        ALU.registers[0] = A
        ALU.registers[1] = B
        ALU.registers[2] = C
        ALU.registers[3] = D

    @staticmethod
    def compare_register(A, B, C, D):
        return ALU.registers[0] == A and \
            ALU.registers[1] == B and \
            ALU.registers[2] == C and \
            ALU.registers[3] == D

    @staticmethod
    def execute(opcode, A, B, C):
        # try:
        ALU.op_codes[opcode](A, B, C)
        # except:
        #     pass

    #  mnemonics
    @staticmethod
    def addr(A, B, C):
        ALU.registers[C] = ALU.registers[A]+ALU.registers[B]

    @staticmethod
    def addi(A, B, C):
        ALU.registers[C] = ALU.registers[A]+B

    @staticmethod
    def mulr(A, B, C):
        ALU.registers[C] = ALU.registers[A]*ALU.registers[B]

    @staticmethod
    def muli(A, B, C):
        ALU.registers[C] = ALU.registers[A]*B

    @staticmethod
    def banr(A, B, C):
        ALU.registers[C] = ALU.registers[A] & ALU.registers[B]

    @staticmethod
    def bani(A, B, C):
        ALU.registers[C] = ALU.registers[A] & B

    @staticmethod
    def borr(A, B, C):
        ALU.registers[C] = ALU.registers[A] | ALU.registers[B]

    @staticmethod
    def bori(A, B, C):
        ALU.registers[C] = ALU.registers[A] | B

    @staticmethod
    def setr(A, B, C):
        ALU.registers[C] = ALU.registers[A]

    @staticmethod
    def seti(A, B, C):
        ALU.registers[C] = A

    @staticmethod
    def gtir(A, B, C):
        ALU.registers[C] = 1 if A > ALU.registers[B] else 0

    @staticmethod
    def gtri(A, B, C):
        ALU.registers[C] = 1 if ALU.registers[A] > B else 0

    @staticmethod
    def gtrr(A, B, C):
        ALU.registers[C] = 1 if ALU.registers[A] > ALU.registers[B] else 0

    @staticmethod
    def eqir(A, B, C):
        ALU.registers[C] = 1 if A == ALU.registers[B] else 0

    @staticmethod
    def eqri(A, B, C):
        ALU.registers[C] = 1 if ALU.registers[A] == B else 0

    @staticmethod
    def eqrr(A, B, C):
        ALU.registers[C] = 1 if ALU.registers[A] == ALU.registers[B] else 0


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def main():
    lines = read_lines("Day_16/inputs/input1")
    if lines is None:
        return 1

    ###############################################
    # Part One
    possible_opcodes = {}
    ALU()
    total_count = 0
    pc = 0
    while len(lines[pc]) > 1:
        m = re.findall("([0-9]+)", lines[pc])
        m1 = re.findall("([0-9]+)", lines[pc+1])
        m2 = re.findall("([0-9]+)", lines[pc+2])
        count = 0
        # print(idx, ' ', m1[0], "could be:")
        possibilities = []
        # try all opcodes
        for i in range(16):
            ALU.set_registers(int(m[0]), int(m[1]), int(m[2]), int(m[3]))
            ALU.execute(i, int(m1[1]), int(m1[2]), int(m1[3]))
            if ALU.compare_register(int(m2[0]), int(m2[1]), int(m2[2]), int(m2[3])):
                count += 1
                possibilities.append(i)
                # print(i, ' ', end='')
        # print()
        # print(count)
        # print()
        if count >= 3:
            total_count += 1

        if m1[0] in possible_opcodes:
            if len(possible_opcodes[m1[0]]) > len(possibilities):
                possible_opcodes[m1[0]] = possibilities
        else:
            possible_opcodes[m1[0]] = possibilities

        pc += 4

    print("Part 1: ", total_count)
    ###############################################
    # Part Two

    # build translation table for opcodes by elimination
    translation_table = {}
    while len(translation_table) != 16:
        for key in possible_opcodes:
            if len(possible_opcodes[key]) == 1:
                translation_table[key] = possible_opcodes[key][0]
                for key2 in possible_opcodes:
                    i = 0
                    while i < len(possible_opcodes[key2]):
                        if possible_opcodes[key2][i] == translation_table[key]:
                            del possible_opcodes[key2][i]
                        i += 1
                break
        #     print(key, real_opcodes[key])
        # print()

    # clear registers
    ALU.set_registers(0, 0, 0, 0)
    # set pc at the begining of an input
    pc += 2
    while pc < len(lines):
        m = re.findall("([0-9]+)", lines[pc])
        ALU.execute(translation_table[m[0]], int(m[1]), int(m[2]), int(m[3]))
        pc += 1
    
    print("Part 2: ", ALU.registers[0])
    return 0


main()
