"""Day 14 problem solver"""


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def contains(nibble, list):
    if len(list) < len(nibble):
        return False
    for idx, element in enumerate(nibble):
        if element != list[idx]:
            return False
    return True


def main():
    lines = read_lines("Day_14/inputs/input1")
    if lines is None:
        return 1

    num = int(lines[0])
    final_scores = [int(x) for x in lines[0]]
    ###############################################
    # Part One

    scores = [3, 7]
    elves = [0, 1]
    while len(scores) < num+10:
        sum = 0
        for elf in elves:
            sum += scores[elf]
        scores += [int(x) for x in str(sum)]
        for idx in range(len(elves)):
            elves[idx] = (elves[idx]+1+scores[elves[idx]]) % len(scores)
    print("Part 1: "+"".join(str(s) for s in scores[-10:]))

    ###############################################
    # Part Two
    scores = [3, 7]
    elves = [0, 1]
    i = 0
    while scores[-len(final_scores):] != final_scores and scores[-len(final_scores) - 1:-1] != final_scores:
        i += 1
        sum = 0
        sum = scores[elves[0]]+scores[elves[1]]
        scores.extend(divmod(sum, 10) if sum >= 10 else (sum,))

        elves[0] = (elves[0]+1+scores[elves[0]]) % len(scores)
        elves[1] = (elves[1]+1+scores[elves[1]]) % len(scores)
        
    print("Part 2: "+str(len(scores)+1-len(final_scores)))
    return 0


main()
