"""Day 9 problem solver"""

import numpy as np
import matplotlib.pyplot as plt
from collections import namedtuple


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def main():
    lines = read_lines("Day_9/inputs/input1")
    if lines is None:
        return 1
    ###############################################
    # Part One

    cave_map = np.zeros((len(lines), len(lines[0])), int)
    basin_map = np.zeros((len(lines), len(lines[0])), int)
    ret = 0

    Point = namedtuple('Point', ['x', 'y'])

    for y, line in enumerate(lines):
        for x, c in enumerate(line):
            cave_map[y][x] = int(c)
    low_points = []
    for y in range(cave_map.shape[0]):
        for x in range(cave_map.shape[1]):
            lower = True
            for p in [Point(-1, 0), Point(1, 0), Point(0, 1), Point(0, -1)]:
                if p.x+x >= cave_map.shape[1] or p.x+x < 0:
                    continue
                if p.y+y >= cave_map.shape[0] or p.y+y < 0:
                    continue
                if cave_map[p.y+y][p.x+x] <= cave_map[y][x]:
                    lower = False
                    break
            if lower:
                ret += 1+cave_map[y][x]
                low_points.append(Point(x, y))
    print(f"Part 1: {ret}")
    ###############################################
    # Part Two
    areas = []
    for i, lp in enumerate(low_points):
        p_cpy = [lp]
        basin_map[lp.y][lp.x] = i+20
        area = 1
        while len(p_cpy) > 0:
            tmp = []
            for x, y in p_cpy:
                for p in [Point(-1, 0), Point(1, 0), Point(0, 1), Point(0, -1)]:
                    if p.x+x >= cave_map.shape[1] or p.x+x < 0:
                        continue
                    if p.y+y >= cave_map.shape[0] or p.y+y < 0:
                        continue
                    if cave_map[p.y+y][p.x+x] > cave_map[y][x]:
                        if cave_map[p.y+y][p.x+x] < 9:
                            if basin_map[p.y+y][p.x+x] == 0:
                                area += 1
                            basin_map[p.y+y][p.x+x] = i+50
                            tmp.append(Point(p.x+x, p.y+y))

            p_cpy = tmp
        areas.append(area)
    areas = sorted(areas)
    ret = areas[-1]*areas[-2]*areas[-3]
    plt.imshow(basin_map)
    plt.show()
    # plt.imshow(cave_map)
    # plt.show()
    print(f"Part 2: {ret}")
    return 0


main()
