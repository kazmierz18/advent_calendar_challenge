/*===============*/
/* Day 11        */
/*===============*/
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

struct Position {
  int x = 0;
  int y = 0;
  int get_distance() {
    int abs_x = std::abs(x);
    int abs_y = std::abs(y);
    int sum = 0;
    while (abs_x > 0) {
      abs_x -= 10;
      abs_y -= 5;
      sum++;
    }
    sum += abs_y / 10;
    return sum;
  }
  void step(const std::string &s) {
    char c_prev;
    for (const auto c : s) {
      if (c == 's') {
        y -= 10;
      } else if (c == 'n') {
        y += 10;
      } else if (c == 'w') {
        x -= 10;
        y += c_prev == 'n' ? -5 : 5;
      } else if (c == 'e') {
        x += 10;
        y += c_prev == 'n' ? -5 : 5;
      }
      c_prev = c;
    }
  }
};

auto split(const std::string &line, const char *delimiter) {
  std::vector<std::string> splitted;
  auto currentIdx = -1;
  do {
    currentIdx++;
    const auto foundIdx = line.find_first_of(delimiter, currentIdx);
    splitted.push_back(line.substr(currentIdx, foundIdx - currentIdx));
    currentIdx = foundIdx;
  } while (currentIdx != std::string::npos);
  return splitted;
}

int main(int argc, char **argv) {
  // read input file
  char *name = nullptr;
  if (argc >= 2) {
    name = argv[1];
  } else {
    return 1;
  }
  std::ifstream file(name);
  std::string tmp;
  std::vector<std::string> lines;
  while (std::getline(file, tmp)) {
    // remove new line characters
    if (*(tmp.end()) == '\n') {
      tmp.pop_back();
    }
    lines.push_back(tmp);
  }
  std::vector<std::string> splitted = split(lines[0], ",");
  //================================================
  Position p;
  for (auto const &item : splitted) {
    p.step(item);
  }
  std::cout << "Part One: " << p.get_distance() << "\n";
  //================================================
  std::cout << "Part Two: "
            << "\n";
  return 0;
}
