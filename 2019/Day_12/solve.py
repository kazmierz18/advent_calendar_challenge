"""Day 12 problem solver"""
import re
from math import gcd  # or can import gcd from `math` in Python 3


def lcm(x, y):
    return x * y // gcd(x, y)


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


class Moon:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
        self.dx = 0
        self.dy = 0
        self.dz = 0

    def simulate(self):
        self.x += self.dx
        self.y += self.dy
        self.z += self.dz

    def potential_energy(self):
        return abs(self.x)+abs(self.y)+abs(self.z)

    def kinetick_energy(self):
        return abs(self.dx)+abs(self.dy)+abs(self.dz)

    def energy(self):
        return self.potential_energy()*self.kinetick_energy()

    def calc_velocity(self, other):
        if self.x > other.x:
            self.dx -= 1
        elif self.x < other.x:
            self.dx += 1

        if self.y > other.y:
            self.dy -= 1
        elif self.y < other.y:
            self.dy += 1

        if self.z > other.z:
            self.dz -= 1
        elif self.z < other.z:
            self.dz += 1


def calc(x, y):
    if x > y:
        return -1
    elif x < y:
        return 1
    return 0


def cycles(xs):
    dx = [0, 0, 0, 0]
    iter = 0
    for idx, x in enumerate(xs):
        for xx in xs:
            dx[idx] += calc(x, xx)
    for i in range(4):
        xs[i] += dx[i]
    iter += 1
    while not all(v == 0 for v in dx):
        for idx, x in enumerate(xs):
            for xx in xs:
                dx[idx] += calc(x, xx)
        for i in range(4):
            xs[i] += dx[i]
        iter += 1

    return iter*2


def main():
    lines = read_lines("Day_12/inputs/input1")
    if lines is None:
        return 1
    ###############################################
    # Part One
    moons = []
    moons_cpy = []
    for line in lines:
        m = re.findall(r'(-?\d+)', line)
        moons.append(Moon(int(m[0]), int(m[1]), int(m[2])))
        moons_cpy.append(Moon(int(m[0]), int(m[1]), int(m[2])))

    for _ in range(1000):
        for moon in moons:
            for m in moons:
                if m != moon:
                    moon.calc_velocity(m)
        for moon in moons:
            moon.simulate()

    total_energy = 0
    for moon in moons:
        total_energy += moon.energy()

    print("Part 1: ", str(total_energy))
    ###############################################
    # Part Two
    xs = [moon.x for moon in moons_cpy]
    cx = cycles(xs)
    ys = [moon.y for moon in moons_cpy]
    cy = cycles(ys)
    zs = [moon.z for moon in moons_cpy]
    cz = cycles(zs)
    first_match = lcm(lcm(cx, cy), cz)

    print("Part 2: " + str(first_match))
    return 0


main()
