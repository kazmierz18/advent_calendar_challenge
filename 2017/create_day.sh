#!/bin/bash

if [ $# -ne 1 ]; then
    echo "Specify day number"
    exit 1
fi
DAY_NUM=$1
DAY_FOLDER_NAME="Day_$DAY_NUM"
if [ ! -d "$DAY_FOLDER_NAME" ]; then
    echo "Creating dir for day $DAY_NUM"
    mkdir "$DAY_FOLDER_NAME"
    
    mkdir "$DAY_FOLDER_NAME/inputs"
    sed -i "s/Day_[[:digit:]]*/$DAY_FOLDER_NAME/g" ../.vscode/tasks.json
    sed -i "s/Day_[[:digit:]]*/$DAY_FOLDER_NAME/g" ../.vscode/launch.json
    
    touch "$DAY_FOLDER_NAME/inputs/input1"
    touch "$DAY_FOLDER_NAME/inputs/input2"
    touch "$DAY_FOLDER_NAME/Challenge.md"
    
    SOLVE_FILE="$DAY_FOLDER_NAME/main.cpp"
    
    {
        echo "/*===============*/" ;
        echo "/* Day $DAY_NUM        */";
        echo "/*===============*/";
        echo "#include <fstream>";
        echo "#include <iostream>";
        echo "#include <string>";
        echo "#include <vector>";
        echo "";
        echo "";
        echo "int main(int argc, char **argv){";
        echo "  // read input file";
        echo "  char *name = nullptr;";
        echo "  if (argc >= 2) {";
        echo "    name = argv[1];";
        echo "  }else{";
        echo "    return 1;";
        echo "  }";
        echo "  std::ifstream file(name);";
        echo "  std::string tmp;";
        echo "  std::vector<std::string> lines;";
        echo "  while (std::getline(file, tmp)) {";
        echo "    // remove new line characters";
        echo "    if (*(tmp.end()) == '\n') {";
        echo "       tmp.pop_back();";
        echo "    }";
        echo "    lines.push_back(tmp);";
        echo "  }";
        echo "  std::cout << lines[0] << \"\n\";";
        echo "  //================================================";
        echo "  std::cout << \"Part One: \" << \"\n\";";
        echo "  //================================================";
        echo "  std::cout << \"Part Two: \" << \"\n\";";
        echo "  return 0;"
        echo "}";
    } > "$SOLVE_FILE"
    
    CMAKE_FILE="$DAY_FOLDER_NAME/CMakeLists.txt"
    {
        echo "project(Day_$DAY_NUM)"
        echo "";
        echo "set(CMAKE_CXX_COMPILER clang++)";
        echo "set(CMAKE_CXX_CLANG_TIDY clang-tidy -checks=-*,readability-*)";
        echo "set(CMAKE_CXX_STANDARD 17)";
        echo "set(CMAKE_CXX_STANDARD_REQUIRED ON)";
        echo "set(CMAKE_CXX_FLAGS_DEBUG \"${CMAKE_CXX_FLAGS_DEBUG} -Wall -O0 -g3 -Wpedantic\")";
        echo "set(CMAKE_CXX_FLAGS_RELEASE \"${CMAKE_CXX_FLAGS_RELEASE} -Wall -O2\")";
        echo "";
        echo "add_executable(solve main.cpp)";
    } > "$CMAKE_FILE"
    mkdir "$DAY_FOLDER_NAME/build"
    cd "$DAY_FOLDER_NAME/build"
    cmake -DCMAKE_BUILD_TYPE=Debug ..
else
    echo "Directory $DAY_FOLDER_NAME already exist"
fi

exit 0