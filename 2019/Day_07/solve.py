"""Day 07 problem solver"""

import copy
from intcode import intcode_cpu, preprocess
from itertools import permutations
from queue import Queue
from threading import Thread


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


class CPU_thread(Thread):
    def __init__(self, cpu, id):
        Thread.__init__(self)
        self.id = id
        self.cpu = cpu

    def run(self):
        self.cpu.run()


def main():
    lines = read_lines("Day_07/inputs/input1")
    if lines is None:
        return 1
    program = preprocess(lines)
    max_v = 0
    ###############################################
    # Part One
    per_list = permutations([0, 1, 2, 3, 4])
    for p in per_list:
        cpus = []
        ths = []
        for x in range(5):
            cpu = intcode_cpu()
            cpu.load_program(program)
            cpu.input = Queue(2)
            cpu.input.put(p[x])
            t = CPU_thread(cpu, x)
            t.daemon = True
            ths.append(t)
            cpus.append(cpu)
        cpus[0].input.put(0)
        cpus[0].output = cpus[1].input
        cpus[1].output = cpus[2].input
        cpus[2].output = cpus[3].input
        cpus[3].output = cpus[4].input
        cpus[4].output = Queue(1)
        for t in ths:
            t.start()
        for t in ths:
            t.join()
        max_v = max([max_v, cpus[4].output.get()])
    print("Part 1: " + str(max_v))
    ###############################################
    # Part Two
    per_list = permutations([5, 6, 7, 8, 9])
    for p in per_list:
        cpus = []
        ths = []
        for x in range(5):
            cpu = intcode_cpu()
            cpu.load_program(program)
            cpu.input = Queue(2)
            cpu.input.put(p[x])
            t = CPU_thread(cpu, x)
            t.daemon = True
            ths.append(t)
            cpus.append(cpu)
        cpus[0].input.put(0)
        cpus[0].output = cpus[1].input
        cpus[1].output = cpus[2].input
        cpus[2].output = cpus[3].input
        cpus[3].output = cpus[4].input
        cpus[4].output = cpus[0].input
        for t in ths:
            t.start()
        for t in ths:
            t.join()
        max_v = max([max_v, cpus[4].output.get()])
    print("Part 2: " + str(max_v))
    return 0


main()
