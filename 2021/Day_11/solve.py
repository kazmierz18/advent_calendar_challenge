"""Day 11 problem solver"""

import numpy as np
from collections import namedtuple
from matplotlib import pyplot as plt


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None

Point = namedtuple('Point', ['x', 'y'])

def correctEdges(x, y, shape):
    edges = [Point(-1, -1), Point(-1, 0), Point(-1, 1), Point(0, 1),
            Point(0, -1), Point(1, -1), Point(1, 0), Point(1, 1)]
    n = []
    for p in edges:
        if p.x+x >= shape[0] or p.x+x < 0:
            continue
        if p.y+y >= shape[1] or p.y+y < 0:
            continue
        n.append(Point(p.x+x, p.y+y))
    return n

def main():
    grid = np.genfromtxt("Day_11/inputs/input1", delimiter=1)
    ###############################################
    # Part One
    ret = 0
    print(grid)
    print()
    near = []
    for i in range(1000):
        for y in range(grid.shape[0]):
            for x in range(grid.shape[1]):
                if grid[y][x] == 9:
                    near+= correctEdges(x,y, grid.shape)
                grid[y][x] += 1
        while len(near)>0:
            tmp= []
            for x,y in near:
                    if grid[y][x] == 9:
                        tmp+= correctEdges(x,y, grid.shape)
                    grid[y][x] += 1
            near= tmp
        s=0
        for y in range(grid.shape[0]):
            for x in range(grid.shape[1]):
                if grid[y][x] > 9:
                    grid[y][x]=0
                    s+=1
        # print(f'{i+1} {s}')
        ret+=s
        if i==100:
            print(f"Part 1: {ret}")
        if s==grid.shape[0]*grid.shape[1]:
            ###############################################
            # Part Two
            print(f"Part 2: {i+1}")
            break

        # plt.imshow(grid)
        # plt.show()
        # print(grid)
        # print()
    return 0


main()
