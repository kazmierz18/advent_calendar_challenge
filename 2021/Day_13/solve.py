"""Day 13 problem solver"""


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def main():
    lines = read_lines("Day_13/inputs/input2")
    if lines is None:
        return 1
    ###############################################
    # Part One
    data = []
    folds =[]
    for line in lines:
        print(line)
        if 'fold' in line:
            folds.append(line)
        elif len(line)>0:
            data.append(line)
    data = list(map(lambda l: list(map(int,l.split(','))),data))
    ret = len(lines)
    print(f"Part 1: {ret}")
    ###############################################
    # Part Two
    print(f"Part 2: {ret}")
    return 0


main()
