/*===============*/
/* Day 18        */
/*===============*/
#include <array>
#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <string>
#include <type_traits>
#include <vector>

struct Instruction {
  std::string mnemonic;
  char x;
  int y;
};

struct CPU {
  int PC = 0;
  std::array<int, 'z' - 'a' + 1> reg;
  int sound;
  std::map<std::string, std::function<void(char, int)>> commands;
  CPU() {
    commands["snd"] = [this](const char x, const int y) { this->snd(x, y); };
    commands["set"] = [this](const char x, const int y) { this->set(x, y); };
    commands["add"] = [this](const char x, const int y) { this->add(x, y); };
    commands["mul"] = [this](const char x, const int y) { this->mul(x, y); };
    commands["mod"] = [this](const char x, const int y) { this->mod(x, y); };
    commands["rcv"] = [this](const char x, const int y) { this->rcv(x, y); };
    commands["jgz"] = [this](const char x, const int y) { this->jgz(x, y); };
  }
  void execute(const Instruction &instruction) {
    commands[instruction.mnemonic](instruction.x, instruction.y);
  }
  void snd(const char x, const int y) {
    sound = reg[x - 'a'];
    PC++;
  }
  void set(const char x, const int y) {
    reg[x - 'a'] = y;
    PC++;
  }
  void add(const char x, const int y) {
    reg[x - 'a'] += y;
    PC++;
  }
  void mul(const char x, const int y) {
    reg[x - 'a'] *= y;
    PC++;
  }
  void mod(const char x, const int y) {
    reg[x - 'a'] = reg[x - 'a'] % y;
    PC++;
  }
  void rcv(const char x, const int y) {
    if (reg[x - 'a']) {
      reg[x - 'a'] = sound;
      PC = -2;
    }
    PC++;
  }
  void jgz(const char x, const int y) {
    if (reg[x - 'a'] > 0) {
      PC += y;
    }
  }
};

auto split(const std::string &line, const char *delimiter) {
  std::vector<std::string> splitted;
  auto currentIdx = -1;
  do {
    currentIdx++;
    const auto foundIdx = line.find_first_of(delimiter, currentIdx);
    splitted.push_back(line.substr(currentIdx, foundIdx - currentIdx));
    currentIdx = foundIdx;
  } while (currentIdx != std::string::npos);
  return splitted;
}

int main(int argc, char **argv) {
  // read input file
  char *name = nullptr;
  if (argc >= 2) {
    name = argv[1];
  } else {
    return 1;
  }
  std::ifstream file(name);
  std::string tmp;
  std::vector<std::string> lines;
  while (std::getline(file, tmp)) {
    // remove new line characters
    if (*(tmp.end()) == '\n') {
      tmp.pop_back();
    }
    lines.push_back(tmp);
  }
  //================================================
  std::vector<Instruction> program;
  program.reserve(sizeof(lines));
  for (const auto &line : lines) {
    auto splitted = split(line, " ");
    auto y = 0;
    if (splitted.size() > 2) {
      y = std::atoi(splitted[2].c_str());
    }
    program.push_back({splitted[0], splitted[1][0], y});
  }
  CPU cpu;
  while (cpu.PC >= 0 && cpu.PC < program.size()) {
    cpu.execute(program[cpu.PC]);
  }
  std::cout << "Part One: " << cpu.sound << "\n";
  //================================================
  std::cout << "Part Two: "
            << "\n";
  return 0;
}
