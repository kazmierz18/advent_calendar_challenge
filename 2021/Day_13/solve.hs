-- {-# LANGUAGE OverloadedStrings #-}
-- import Data.Text
import Data.List.Split (splitOn)

main :: IO ()
main = do
    content <- readFile "Day_13/inputs/input2"
    [points, folds] <- splitOn "\n\n" content
    print points




-- parse_file content = Prelude.filter isXYPair content

-- isXYPair l = isInfixOf ","  (pack l)

-- isFold l = isPrefixOf "fold" 