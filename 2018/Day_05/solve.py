"""Day 5 problem solver"""


def remove_unit(remove, polymer):
    unit_to_remove = chr(remove)
    upper_unit_to_remove = unit_to_remove.upper()
    i = 0
    while i < len(polymer)-1:
        if polymer[i] == unit_to_remove or polymer[i] == upper_unit_to_remove:
            del polymer[i]
            i -= 1
        else:
            i += 1


def react_polymer(polymer):
    i = 0
    while i < len(polymer)-1:
        i_plus_1 = i+1
        if polymer[i] != polymer[i_plus_1] and (polymer[i] == polymer[i_plus_1].upper()
                                                or polymer[i] == polymer[i_plus_1].lower()):
            del polymer[i_plus_1]
            del polymer[i]
            i -= 1
        else:
            i += 1


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def main():
    lines = read_lines("Day_05/inputs/input1")
    if lines is None:
        return 1

    ##########################################
    polymer = list(lines[0])
    # Part One
    react_polymer(polymer)
    print("Part 1: ", len(polymer))

    ##########################################
    # Part Two
    best = 2 << 32
    for unit in range(ord('a'), ord('z')+1):
        polymer = list(lines[0])
        remove_unit(unit, polymer)
        react_polymer(polymer)
        best = min(best, len(polymer))

    print("Part 2: ", best)

    return 0


main()
