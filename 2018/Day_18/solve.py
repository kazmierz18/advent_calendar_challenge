"""Day 18 problem solver"""

import copy


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def print_map(map):
    for m in map:
        print(''.join(m))


def main():
    lines = read_lines("Day_18/inputs/input1")
    if lines is None:
        return 1
    map = []

    for line in lines:
        line = '.'+line+'.'
        map.append(list(line))
    map.insert(0, ['.' for x in range(len(map[0]))])
    map.append(['.' for x in range(len(map[0]))])
    # print_map(map)
    next_rounds = []
    prev_start=0
    start_of_cycle=0
    count=0
    for i in range(1000000000):
        map2 = copy.deepcopy(map)
        for y in range(1, len(map)-1):
            for x in range(1, len(map[0])-1):
                if map[y][x] == '.':
                    trees = 0
                    for ay in range(y-1, y+2):
                        for ax in range(x-1, x+2):
                            if map[ay][ax] == '|':
                                trees += 1
                    if trees >= 3:
                        map2[y][x] = '|'
                elif map[y][x] == '|':
                    lumbers = 0
                    for ay in range(y-1, y+2):
                        for ax in range(x-1, x+2):
                            if map[ay][ax] == '#':
                                lumbers += 1
                    if lumbers >= 3:
                        map2[y][x] = '#'
                else:
                    lumbers = 0
                    trees = 0
                    for ay in range(y-1, y+2):
                        for ax in range(x-1, x+2):
                            if map[ay][ax] == '#':
                                lumbers += 1
                            if map[ay][ax] == '|':
                                trees += 1
                    if lumbers >= 2 and trees >= 1:
                        map2[y][x] = '#'
                    else:
                        map2[y][x] = '.'
        map = map2
        lumbers = 0
        trees = 0
        for y in range(1, len(map)):
            for x in range(1, len(map[0])):
                if map[y][x] == '#':
                    lumbers += 1
                if map[y][x] == '|':
                    trees += 1
        # print(i, lumbers*trees)
        lumbers = 0
        trees = 0
        for y in range(1, len(map)):
            for x in range(1, len(map[0])):
                if map[y][x] == '#':
                    lumbers += 1
                if map[y][x] == '|':
                    trees += 1
        next_rounds.append(lumbers*trees)
        if len(next_rounds) > 3:
            if next_rounds[-1] in next_rounds[prev_start:-2]:
                start_of_cycle = next_rounds.index(next_rounds[-1])
                # print(i, lumbers*trees, start_of_cycle)
                if prev_start==start_of_cycle-1:
                    count+=1
                    if count==3:
                        break
                prev_start=start_of_cycle
                
               

    ###############################################
    # Part One
    print("Part 1: ", next_rounds[9])
    ###############################################
    # Part Two
    remaining_time=1000000000-start_of_cycle-1
    period=len(next_rounds)-start_of_cycle-1
    print("Part 2: ", next_rounds[remaining_time % period+start_of_cycle])
    return 0


main()
