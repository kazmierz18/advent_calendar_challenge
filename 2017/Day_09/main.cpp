/*===============*/
/* Day 09        */
/*===============*/
#include <algorithm>
#include <fstream>
#include <iostream>
#include <memory>
#include <stack>
#include <string>
#include <vector>

struct Group {
  std::string content;
  std::vector<std::shared_ptr<Group>> subGroups;
  int get_count(const int level) const {
    if (subGroups.empty()) {
      return level;
    }
    auto sum = 0;
    for (const auto &sg : subGroups) {
      sum += sg->get_count(level + 1);
    }
    return sum + level;
  }
};

const char token_cancel = '!';
const char token_open_group = '{';
const char token_close_group = '}';
const char token_open_garbage = '<';
const char token_close_garbage = '>';

struct Parser {
  int garbage_sum = 0;
  std::vector<std::shared_ptr<Group>> groups;
  enum State {
    READ,
    NEGATED,
    GARBAGE,
  };
  State state;
  State prevState;
  int count_groups() const {
    auto sum = 0;
    for (const auto &g : groups) {
      sum += g->get_count(1);
    }
    return sum;
  }
  Parser(const std::string &input) {
    std::stack<std::shared_ptr<Group>> stack;
    std::shared_ptr<Group> current;
    state = READ;
    prevState = READ;
    for (const auto c : input) {
      switch (state) {
      case READ:
        if (c == token_open_group) {
          // std::cerr << "Open Group\n";
          auto g = std::shared_ptr<Group>(new Group());
          if (current) {
            current->subGroups.push_back(g);
            stack.push(current);
          } else {
            groups.push_back(g);
          }
          current = g;
        }
        if (c == token_close_group) {
          // std::cerr << "Close Group\n";
          if (stack.size()) {
            current = stack.top();
            stack.pop();
          } else {
            current = nullptr;
          }
          state = READ;
        }
        if (c == token_cancel) {
          // std::cerr << "Negator\n";
          prevState = READ;
          state = NEGATED;
        }
        if (c == token_open_garbage) {
          // std::cerr << "Open Garbage\n";
          state = GARBAGE;
        }
        break;
      case GARBAGE:
        if (c == token_close_garbage) {
          // std::cerr << "Close Garbage\n";
          state = READ;
        } else if (c == token_cancel) {
          // std::cerr << "Negator\n";
          state = NEGATED;
          prevState = GARBAGE;
        } else {
          garbage_sum++;
        }
        break;
      case NEGATED:
        state = prevState;
        break;
      }
    }
  }
};

int main(int argc, char **argv) {
  // read input file
  char *name = nullptr;
  if (argc >= 2) {
    name = argv[1];
  } else {
    return 1;
  }
  std::ifstream file(name);
  std::string tmp;
  std::vector<std::string> lines;
  while (std::getline(file, tmp)) {
    // remove new line characters
    if (*(tmp.end()) == '\n') {
      tmp.pop_back();
    }
    lines.push_back(tmp);
  }
  //================================================
  Parser p(lines[0]);
  std::cout << "Part One: " << p.count_groups() << "\n";
  //================================================
  std::cout << "Part Two: " << p.garbage_sum << "\n";
  return 0;
}
