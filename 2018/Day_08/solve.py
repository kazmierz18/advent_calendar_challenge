"""Day 8 problem solver"""


import copy


class Node:
    sum_of_metadata=0
    def __init__(self, data):
        self.children_len = int(data[0])
        del data[0]
        self.metadata_len = int(data[0])
        del data[0]
        self.nodes = []
        self.add_children(data)
        self.metadata = copy.deepcopy(data[0:self.metadata_len])
        for i in range(self.metadata_len):
            del data[0]
        for md in self.metadata:
            Node.sum_of_metadata+=int(md)
    
    def calculate_sum(self):
        self.sum=0
        if self.children_len==0:
            self.sum=sum(int(x) for x in self.metadata)
        else:
            for md in self.metadata:
                idx=int(md)-1
                if idx<self.children_len:
                    self.sum+=self.nodes[idx].calculate_sum()
        return self.sum


    def add_children(self, data):
        for i in range(self.children_len):
            self.nodes.append(Node(data))


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def main():
    lines = read_lines("Day_08/inputs/input1")
    if lines is None:
        return 1
    values = lines[0].split(' ')
    node = Node(values)

    ###############################################
    # Part One
    print("Part 1: ", Node.sum_of_metadata)
        ###############################################
    # Part Two
    print("Part 2: ", node.calculate_sum())
    return 0


main()
