/*===============*/
/* Day 03        */
/*===============*/
#include <fstream>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

struct point {
  int x;
  int y;
  point operator+(const point &p) const { return point{x + p.x, y + p.y}; }
  inline bool in_array(const int length, const int width) const {
    return x >= 0 && x < length && y >= 0 && y < width;
  }
};

int m_distance(const point &p1, const point &p2) {
  return std::abs(p1.x - p2.x) + std::abs(p1.y - p2.y);
}

// const constexpr int input = 12;
const constexpr int input = 312051;
constexpr int get_size(const int input, const int size) {
  if (input <= size * size) {
    return size;
  } else {
    return get_size(input, size + 2);
  }
}
const constexpr int size = get_size(input, 1);

template <typename T, std::size_t S>
using matrix = std::array<std::array<T, S>, S>;

void print_array(matrix<int, size> const &array) {
  for (auto const &row : array) {
    for (auto const &p : row) {

      std::cout << p << " ";
    }
    std::cout << "\n";
  }
  std::cout << "\n";
}

enum direction {
  RIGHT,
  UP,
  LEFT,
  DOWN,
  END,
};

inline direction &operator++(direction &dir, int) {
  const int i = static_cast<int>(dir);
  dir = static_cast<direction>((i + 1) % END);
  return dir;
}

struct gen {
  virtual int operator()(matrix<int, size> &array, const int idx,
                         [[maybe_unused]] const point &p) const {
    return idx;
  }
};

struct sum_gen : public gen {
  int operator()(matrix<int, size> &array, const int idx,
                 const point &p) const override {
    if (idx == 1) {
      return 1;
    }
    auto sum = 0;
    const std::array<point, 8> points = {
        point{-1, -1}, point{-1, 0}, point{-1, 1}, point{0, -1},
        point{0, 1},   point{1, -1}, point{1, 0},  point{1, 1}};
    for (auto const &pi : points) {
      auto tmpPoint = pi + p;
      if (tmpPoint.in_array(size, size)) {
        sum += array[tmpPoint.y][tmpPoint.x];
      }
    }
    return sum;
  }
};

struct spiral {
  direction dir = RIGHT;
  int x = size / 2;
  int y = size / 2;
  int idx = 1;
  int idx2 = 0;
  int toRepeat = 1;
  bool repeat = true;
  int value = 0;
  spiral(matrix<int, size> &array, const gen &g) {
    while (idx <= size * size) {
      array[y][x] = g(array, idx, {x, y});
      if (array[y][x] > input && value == 0) {
        value = array[y][x];
      }
      switch (dir) {
      case LEFT:
        x--;
        break;
      case RIGHT:
        x++;
        break;
      case UP:
        y--;
        break;
      case DOWN:
        y++;
        break;
      case END:
        break;
      }
      idx++;
      idx2++;
      if (idx2 == toRepeat) {
        dir++;
        idx2 = 0;
        if (not repeat) {
          repeat = true;
          toRepeat++;
        } else {
          repeat = false;
        }
      }
    }
  }
};

int main(int argc, char **argv) {
  //================================================
  std::array<std::array<int, size>, size> array;
  spiral s(array, gen());
  auto distance = 0;
  for (auto x = 0; x < size; x++) {
    for (auto y = 0; y < size; y++) {
      if (array[x][y] == input) {
        distance = m_distance({x, y}, {size / 2, size / 2});
        break;
      }
    }
  }
  std::cout << "Part One: " << distance << "\n";
  //================================================
  for (auto &row : array) {
    row.fill(0);
  }
  spiral s2(array, sum_gen());
  std::cout << "Part Two: " << s2.value << "\n";
  return 0;
}
