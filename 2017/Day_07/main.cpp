/*===============*/
/* Day 07        */
/*===============*/
#include <algorithm>
#include <fstream>
#include <iostream>
#include <memory>
#include <regex>
#include <string>
#include <vector>

template <typename _ForwardIterator, typename _BinaryPredicate>
inline _ForwardIterator find_different(_ForwardIterator __first,
                                       _ForwardIterator __last,
                                       _BinaryPredicate __binary_pred) {
  auto different = __first;
  for (auto i = __first + 1; i != __last; i++) {
    if (__binary_pred(*different, *i)) {
      if (i + 1 != __last) {
        if (__binary_pred(*different, *(i + 1))) {
          return different;
        } else {
          return i;
        }
      }
      return i;
    }
  }
  return __last;
}

struct disc {
  int uneven = 0;
  int sum_children = 0;
  disc() = default;
  std::string name;
  int weight = 0;
  std::weak_ptr<disc> parent;
  std::vector<std::weak_ptr<disc>> children;
  int get_sum_of_children() {
    auto sum = 0;
    for (auto const &child_ptr : children) {
      auto child = child_ptr.lock();
      sum += child->get_sum_of_children();
    }
    sum_children = sum;
    return sum_children + weight;
  }
  const disc *get_unstable() {
    if (children.empty()) {
      return nullptr;
    }
    std::vector<int> values;
    for (auto const &child_ptr : children) {
      auto child = child_ptr.lock();
      if (auto object = child->get_unstable(); object != nullptr) {
        return object;
      };
      values.push_back(child->sum_children + child->weight);
    }
    auto found = std::adjacent_find(
        values.begin(), values.end(),
        [](const auto &lhs, const auto &rhs) { return lhs != rhs; });
    if (found != values.end()) {
      uneven = *found - *(found + 1);
      return this;
    }
    return nullptr;
  }
};

int main(int argc, char **argv) {
  // read input file
  char *name = nullptr;
  if (argc >= 2) {
    name = argv[1];
  } else {
    return 1;
  }
  std::ifstream file(name);
  std::string tmp;
  std::vector<std::string> lines;
  while (std::getline(file, tmp)) {
    // remove new line characters
    if (*(tmp.end()) == '\n') {
      tmp.pop_back();
    }
    lines.push_back(tmp);
  }
  //================================================
  std::vector<std::shared_ptr<disc>> discs;
  discs.reserve(lines.size());
  for (auto const &line : lines) {
    std::regex expression("\\w+");
    std::smatch sMatch;
    std::string input = line;
    std::string name;
    std::shared_ptr<disc> d;
    if (std::regex_search(input, sMatch, expression)) {
      name = sMatch[0];
      input = sMatch.suffix().str();
    } else {
      // if there is no name then skip it
      continue;
    }
    auto found =
        std::find_if(discs.begin(), discs.end(), [&name](const auto &object) {
          return object->name == name;
        });
    if (found != discs.end()) {
      d = *found;
    } else {
      d = std::make_shared<disc>(disc());
      discs.emplace_back(d);
    }
    d->name = name;
    if (std::regex_search(input, sMatch, expression)) {
      std::string tmp = sMatch[0];
      d->weight = atoi(tmp.c_str());
      input = sMatch.suffix().str();
    }
    while (std::regex_search(input, sMatch, expression)) {
      std::string name = sMatch[0];
      const auto found =
          std::find_if(discs.begin(), discs.end(), [&name](const auto &object) {
            return object->name == name;
          });
      std::shared_ptr<disc> d2;
      if (found != discs.end()) {
        d2 = *found;
      } else {
        d2 = std::make_shared<disc>();
        discs.push_back(d2);
      }
      d2->name = name;
      d2->parent = d;
      d->children.push_back(d2);
      input = sMatch.suffix().str();
    }
  }
  auto base = std::find_if(discs.begin(), discs.end(), [](const auto &obj) {
    return obj->parent.expired();
  });

  if (base != discs.end()) {
    std::cout << "Part One: " << (*base)->name << "\n";
    //================================================
    (*base)->get_sum_of_children();
    auto unstable = (*base)->get_unstable();
    if (unstable != nullptr) {
      std::vector<std::pair<int, std::weak_ptr<disc>>> vec;
      for (auto const &child_ptr : unstable->children) {
        auto child = child_ptr.lock();
        auto const sum = child->sum_children + child->weight;
        vec.emplace_back(sum, child_ptr);
      }
      auto d = find_different(vec.begin(), vec.end(),
                              [](decltype(vec)::value_type const &lhs,
                                 decltype(vec)::value_type const &rhs) {
                                return lhs.first != rhs.first;
                              });
      std::cout << "Part Two: " << (*d).second.lock()->weight + unstable->uneven << "\n";
    }
  }
  return 0;
}
