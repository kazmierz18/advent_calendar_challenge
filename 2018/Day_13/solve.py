"""Day 13 problem solver"""

import numpy as np


class Cart:
    crash_x = 0
    crash_y = 0
    cart_id_n = ord('A')

    def __init__(self, x, y, vx, vy):
        self.x = x
        self.y = y
        self.vx = vx
        self.vy = vy
        self.state = 0
        self.was_before = '-' if x else '|'
        self.cart_id = chr(Cart.cart_id_n)
        Cart.cart_id_n += 1
        self.crashed = False

    def has_crashed(self, road, carts):
        if road[self.y][self.x] >= 'A' and road[self.y][self.x] <= 'Z':
            # print('Crash ', self.cart_id, ' with ',  road[self.y][self.x])
            other = next(c for c in carts if (c.cart_id == road[self.y][self.x]))
            road[self.y][self.x] = other.was_before
            Cart.crash_x = self.x
            Cart.crash_y = self.y
            other.crashed = True
            self.crashed = True
            return True
        return False

    def drive(self, road, carts):
        if self.crashed:
            return True
        road[self.y][self.x] = self.was_before
        self.x += self.vx
        self.y += self.vy

        if self.has_crashed(road, carts):
            return True

        if self.was_before>='A' and self.was_before<='Z':
            pass
    
        if road[self.y][self.x] == '+':
            if self.state == 0:
                # left
                if self.vx == -1:
                    self.vx = 0
                    self.vy = 1
                elif self.vx == 1:
                    self.vx = 0
                    self.vy = -1
                elif self.vy == 1:
                    self.vx = 1
                    self.vy = 0
                else:
                    self.vx = -1
                    self.vy = 0

            elif self.state == 2:
                # right
                if self.vx == -1:
                    self.vx = 0
                    self.vy = -1
                elif self.vx == 1:
                    self.vx = 0
                    self.vy = 1
                elif self.vy == 1:
                    self.vx = -1
                    self.vy = 0
                else:
                    self.vx = 1
                    self.vy = 0

            self.state = (self.state+1) % 3
        if road[self.y][self.x] == '\\':
            if self.vx == -1:
                self.vx = 0
                self.vy = -1
            elif self.vx == 1:
                self.vx = 0
                self.vy = 1
            elif self.vy == 1:
                self.vx = 1
                self.vy = 0
            else:
                self.vx = -1
                self.vy = 0

        if road[self.y][self.x] == '/':
            if self.vx == 1:
                self.vx = 0
                self.vy = -1
            elif self.vx == -1:
                self.vx = 0
                self.vy = 1
            elif self.vy == 1:
                self.vx = -1
                self.vy = 0
            else:
                self.vx = 1
                self.vy = 0

        self.was_before = road[self.y][self.x]
        road[self.y][self.x] = self.cart_id
        return False

def print_area_road(road,x,y,size):
    for r in road[y-size:y+size+1]:
        print(''.join(r[x-size:x+size+1]))
    print()

def print_road(road):
    for r in road:
        print(''.join(r))
    print()


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def main():
    lines = read_lines("Day_13/inputs/input1")
    if lines is None:
        return 1

    # road = np.chararray([len(lines[0]), len(lines)])
    road = []
    carts = []
    for y, line in enumerate(lines):
        
        road.append(list(line))
        for x, l in enumerate(line):
            if l == '>':
                carts.append(Cart(x, y, 1, 0))
                road[-1][x]=carts[-1].cart_id
            elif l == '<':
                carts.append(Cart(x, y, -1, 0))
                road[-1][x]=carts[-1].cart_id
            elif l == 'v':
                carts.append(Cart(x, y, 0, 1))
                road[-1][x]=carts[-1].cart_id
            elif l == '^':
                carts.append(Cart(x, y, 0, -1))
                road[-1][x]=carts[-1].cart_id

    # for cart in carts:
    #     print_area_road(road,cart.x,cart.y,3)
    ###############################################
    # Part One

    # print_road(road)
    crashed = False
    while not crashed:
        for cart in carts:
            if cart.drive(road, carts):
                crashed = True
        carts = [c for c in carts if not c.crashed]
        # print_road(road)
    # print_road(road)

    print("Part 1: ", Cart.crash_x, ',', Cart.crash_y)

    ###############################################
    # Part Two
    while len(carts) > 1:
        for cart in carts:
            cart.drive(road, carts)
        carts = [c for c in carts if not c.crashed]
        # print_road(road)
        # for cart in carts:
        #     print(cart.cart_id,end='')
        # print()
    print("Part 2: ", carts[0].x, ',', carts[0].y)
    return 0


main()
