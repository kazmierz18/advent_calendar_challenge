#!/bin/bash

if [ $# -ne 1 ]; then
    echo "Specify day number"
    exit 1
fi

DAY_NUM=$1
DAY_FOLDER_NAME="Day_$DAY_NUM"
sed -i "s/Day_[[:digit:]]*/$DAY_FOLDER_NAME/g" ../.vscode/tasks.json
sed -i "s/Day_[[:digit:]]*/$DAY_FOLDER_NAME/g" ../.vscode/launch.json