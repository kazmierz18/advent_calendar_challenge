/*===============*/
/* Day 04        */
/*===============*/
#include <algorithm>
#include <array>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

auto split(const std::string &line, const char *delimiter) {
  std::vector<std::string> splitted;
  auto currentIdx = -1;
  do {
    currentIdx++;
    const auto foundIdx = line.find_first_of(delimiter, currentIdx);
    splitted.push_back(line.substr(currentIdx, foundIdx - currentIdx));
    currentIdx = foundIdx;
  } while (currentIdx != std::string::npos);
  return splitted;
}

int main(int argc, char **argv) {
  // read input file
  char *name = nullptr;
  if (argc >= 2) {
    name = argv[1];
  } else {
    return 1;
  }
  std::ifstream file(name);
  std::string tmp;
  std::vector<std::string> lines;
  while (std::getline(file, tmp)) {
    // remove new line characters
    if (*(tmp.end()) == '\n') {
      tmp.pop_back();
    }
    lines.push_back(tmp);
  }
  //================================================
  auto sum = 0;
  for (const auto &line : lines) {
    auto splitted = split(line, "\t ");
    std::sort(splitted.begin(), splitted.end());
    const auto found = std::adjacent_find(splitted.begin(), splitted.end());
    if (found == splitted.end()) {
      sum++;
    }
  }
  std::cout << "Part One: " << sum << "\n";
  //================================================
  sum = 0;
  for (const auto &line : lines) {
    auto splitted = split(line, "\t ");
    for (auto &word : splitted) {
      std::sort(word.begin(), word.end());
    }
    std::sort(splitted.begin(), splitted.end());
    const auto found = std::adjacent_find(splitted.begin(), splitted.end());
    if (found == splitted.end()) {
      sum++;
    }
  }
  std::cout << "Part Two: " << sum << "\n";
  return 0;
}
