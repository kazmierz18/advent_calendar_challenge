"""Day 03 problem solver"""


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


class Line:
    def __init__(self, x, y, x2, y2):
        self.x1 = x
        self.y1 = y
        self.x2 = x2
        self.y2 = y2
        self.length = abs(x-x2)+abs(y-y2)

    def intersect(self, other):
        x1 = self.x1
        y1 = self.y1

        x2 = self.x2
        y2 = self.y2

        x3 = other.x1
        y3 = other.y1

        x4 = other.x2
        y4 = other.y2

        # print(str(x1)+","+str(y1) + " "+str(x2)+","+str(y2) +
        #       " - " + str(x3)+","+str(y3) + " "+str(x4)+","+str(y4))
        den = (x1-x2)*(y3-y4)-(y1-y2)*(x3-x4)
        if den == 0:
            return None
        t = ((x1-x3)*(y3-y4)-(y1-y3)*(x3-x4))/den
        u = -((x1-x2)*(y1-y3)-(y1-y2)*(x1-x3))/den
        if u > 0 and u < 1 and t > 0 and t < 1:
            cross = (x3+u*(x4-x3), y3+u*(y4-y3))
        else:
            return None
        return cross


def manhattan_dist_00(point):
    return abs(point[0])+abs(point[1])


def create_wire(wire):
    x = 0
    y = 0
    lines = []
    for i in wire:
        dir = i[0]
        val = int(i[1:])
        dx = x
        dy = y
        if dir == 'R':
            dx += val
        elif dir == 'U':
            dy += val
        elif dir == 'D':
            dy -= val
        elif dir == 'L':
            dx -= val
        lines.append(Line(x, y, dx, dy))
        x = dx
        y = dy
    return lines


def is_on_line(point, segment):
    den = abs(segment.x2-segment.x1)
    if den == 0:
        if segment.y1 > segment.y2:
            return segment.x1 == point[0] and segment.y1 > point[1] and segment.y2 < point[1]
        else:
            return segment.x1 == point[0] and segment.y1 < point[1] and segment.y2 > point[1]
    a = abs(segment.y2-segment.y1)/den
    b = -a*segment.x1+segment.y1
    return (a*point[0]+b) == point[1]


def main():
    lines = read_lines("Day_03/inputs/input1")
    if lines is None:
        return 1
    ###############################################
    # Part One
    wire1 = create_wire(lines[0].split(','))
    wire2 = create_wire(lines[1].split(','))

    crosses = []
    for a in wire1:
        for b in wire2:
            cross = a.intersect(b)
            if cross:
                crosses.append(cross)
    closest = min(crosses, key=lambda x: manhattan_dist_00(x))
    print("Part 1: ", manhattan_dist_00(closest))
    ###############################################
    # Part Two
    sums = []
    for c in crosses:
        sum = 0
        for w in wire1:
            if is_on_line(c, w):
                sum += abs(w.x1-c[0])+abs(w.y1 - c[1])
                break
            else:
                sum += w.length
        for w in wire2:
            if is_on_line(c, w):
                sum += abs(w.x1-c[0])+abs(w.y1 - c[1])
                break
            else:
                sum += w.length
        sums.append(sum)

    print("Part 2: " + str(min(sums)))
    return 0


main()
