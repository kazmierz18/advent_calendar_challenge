"""Day 23 problem solver"""

import re
import heapq
from dataclasses import dataclass

QUADRANTS = [(0, 0, 0), (0, 0, 1), (0, 1, 0), (0, 1, 1),
             (1, 0, 0), (1, 0, 1), (1, 1, 0), (1, 1, 1)]


class Box:
    def __init__(self, bots, size, corner_pos):
        self.size = size
        self.corner_pos = corner_pos
        self.dist_to_0 = self.distance_to_0()
        self.bots_in_box = sum(1 for b in bots if self.in_box(b))

    def __lt__(self, other):
        # we use gt > instead lt < because we want to invert the result
        if self.bots_in_box == other.bots_in_box:
            if self.size == other.size:
                return self.dist_to_0 >= other.dist_to_0
            else:
                return self.size > other.size
        else:
            return self.bots_in_box > other.bots_in_box

    def distance_to_0(self):
        return abs(self.corner_pos[0][0])+abs(self.corner_pos[0][1])+abs(self.corner_pos[0][2])

    def in_box(self, bot):
        d = 0

        boxlow, boxhigh = self.corner_pos[0][0], self.corner_pos[1][0] - 1
        d += abs(bot.x - boxlow) + abs(bot.x - boxhigh)-(boxhigh - boxlow)

        boxlow, boxhigh = self.corner_pos[0][1], self.corner_pos[1][1] - 1
        d += abs(bot.y - boxlow) + abs(bot.y - boxhigh)-(boxhigh - boxlow)

        boxlow, boxhigh = self.corner_pos[0][2], self.corner_pos[1][2] - 1
        d += abs(bot.z - boxlow) + abs(bot.z - boxhigh)-(boxhigh - boxlow)

        d //= 2
        return d <= bot.r

@dataclass
class Nanobot:
    max_x = 0
    max_y = 0
    max_z = 0

    x: int
    y: int
    z: int
    r: int

    def __post_init__(self):
        Nanobot.max_x = max(Nanobot.max_x, abs(self.x))
        Nanobot.max_y = max(Nanobot.max_y, abs(self.y))
        Nanobot.max_z = max(Nanobot.max_z, abs(self.z))

    def distance(self, other):
        dx = abs(self.x-other.x)
        dy = abs(self.y-other.y)
        dz = abs(self.z-other.z)
        return dx+dy+dz

    def in_range(self, other):
        return True if self.distance(other) <= float(self.r) else False


def intersect_count(box, bots):
    return


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def main():
    lines = read_lines("Day_23/inputs/input1")
    if lines is None:
        return 1

    bots = []
    for line in lines:
        m = re.findall("([+-]?[0-9]+)", line)
        bots.append(Nanobot(int(m[0]), int(m[1]), int(m[2]), int(m[3])))

    bots.sort(key=lambda x: x.r, reverse=True)

    ###############################################
    # Part One
    in_range_cnt = 1
    for b in bots[1:]:
        if bots[0].in_range(b):
            in_range_cnt += 1
    print("Part 1: ", in_range_cnt)
    ###############################################
    # Part Two
    max_abs_cord = max([Nanobot.max_x, Nanobot.max_y, Nanobot.max_z])
    box_size = 1
    while box_size <= max_abs_cord:
        box_size *= 2

    workheap = [Box(bots, 2*box_size, ((-box_size, -box_size, -
                                        box_size), (box_size, box_size, box_size)))]
    current = None
    while workheap:
        current = heapq.heappop(workheap)
        if current.size == 1:
            break
        new_size = current.size // 2
        for octant in QUADRANTS:
            corner_0 = tuple(
                current.corner_pos[0][i] + new_size * octant[i] for i in (0, 1, 2))
            corner_1 = tuple(corner_0[i] + new_size for i in (0, 1, 2))
            newbox = (corner_0, corner_1)
            heapq.heappush(workheap, Box(bots, new_size, newbox))

    print("Part 2: ", current.dist_to_0)
    return 0


main()
