"""Day 06 problem solver"""
from heapq import heappop, heappush
import sys


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def find_path(graph, source, dest):
    q = [(0, source)]
    seen = set()
    costs = {source: 0}
    while q:
        current_cost, current_name = heappop(q)
        if current_name not in seen:
            seen.add(current_name)
            if current_name == dest:
                return current_cost
        for child in graph[current_name].neighbors:
            if child.name in seen:
                continue
            prev = costs.get(child.name, sys.maxsize)
            next = current_cost+1
            if next < prev:
                costs[child.name] = next
                heappush(q, (next, child.name))
    return 0


class Orbit:
    def __init__(self, name):
        self.name = name
        self.children = []
        self.parent = None
        self.neighbors = []

    def add_child(self, child):
        self.children.append(child)
        self.neighbors.append(child)

    def set_parent(self, parent):
        self.parent = parent
        self.neighbors.append(parent)

    def count(self):
        if self.parent:
            return self.parent.count()+1
        return 0


def main():
    lines = read_lines("Day_06/inputs/input1")
    if lines is None:
        return 1
    ###############################################
    # Part One
    orbits = {}
    for line in lines:
        parts = line.split(')')
        o = None
        try:
            o = orbits[parts[0]]
        except:
            o = Orbit(parts[0])
            orbits[parts[0]] = o
        try:
            ch = orbits[parts[1]]
            ch.set_parent(o)
            o.add_child(ch)
        except:
            ch = Orbit(parts[1])
            orbits[parts[1]] = ch
            ch.set_parent(o)
            o.add_child(ch)
    sum = 0
    for key in orbits:
        sum += orbits[key].count()

    print("Part 1: " + str(sum))
    ###############################################
    # Part Two

    print("Part 2: " + str(find_path(orbits, "YOU", "SAN")-2))
    return 0


main()
