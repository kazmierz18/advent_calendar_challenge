"""Day 4 problem solver"""

from datetime import datetime
import operator


class Shift:
    def __init__(self, id):
        self.sleeps = []
        self.wakes_up = []
        self.breaks = []

    def add_wake_up(self, time):
        self.wakes_up.append(time)

    def add_sleep(self, time):
        self.sleeps.append(time)

    def calculate_breaks(self):
        total_break_time = 0
        for idx, wake in enumerate(self.wakes_up):
            if self.sleeps[idx].hour < 1:
                time_diff = wake-self.sleeps[idx]
                minutes = int(time_diff.total_seconds())//60
                total_break_time += minutes
                self.breaks.append((minutes, self.sleeps[idx].minute))
            else:
                break
        return total_break_time

    def find_overlaping_breaks(self, t):
        for b in self.breaks:
            for x in range(0, b[0]):
                t[b[1]+x] += 1
        return t


class Guard:
    def __init__(self, id):
        self.id=id
        self.shifts=[]
        self.last_shift=None

    def start_shift(self, time):
        self.shifts.append(Shift(self.parse_datetime(time)))
        self.last_shift=self.shifts[-1]

    def add_to_shift(self, time, action):
        d_time=self.parse_datetime(time)

        if action == 'wakes':
            self.last_shift.add_wake_up(d_time)
        elif action == 'falls':
            self.last_shift.add_sleep(d_time)
        else:
            print("Action not known")

    def parse_datetime(self, time):
        return datetime.strptime(time, '%Y-%m-%d %H:%M')

    def calculate_timetable(self):
        self.total_breaks=0
        for shift in self.shifts:
            self.total_breaks += shift.calculate_breaks()

        self.t=[0]*60
        for shift in self.shifts:
            self.t=shift.find_overlaping_breaks(self.t)
        self.max_t_idx, self.max_t=max(
            enumerate(self.t), key=operator.itemgetter(1))


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines=file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def main():
    lines=read_lines("Day_04/inputs/input2")
    if lines is None:
        return 1
    lines.sort()
    
    ###############################################
    # Prepare model
    guards=[]
    guard=None
    for line in lines:
        time=line[1:17]
        parts=line[19:].split(' ')
        if parts[0] == 'Guard':
            id=int(parts[1][1:])
            guard=next((g for g in guards if id == g.id), None)
            if guard is None:
                guards.append(Guard(id))
                guard=guards[-1]
            guard.start_shift(time)
        else:
            guard.add_to_shift(time, parts[0])

    for g in guards:
        g.calculate_timetable()

    ###############################################
    # Part One
    guards.sort(key=lambda x: x.total_breaks)
    print("Part 1: ", guards[-1].id*guards[-1].max_t_idx)

    ###############################################
    # Part Two
    guards.sort(key=lambda x: x.max_t)
    print("Part 2: ", guards[-1].id*guards[-1].max_t_idx)
    return 0


main()
