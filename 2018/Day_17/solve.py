"""Day 17 problem solver"""

import re
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image


class Source:

    def __init__(self, x, y):
        self.x = x
        self.y = y


class Line:
    max_X = 0
    min_X = 2 << 32
    max_Y = 0
    min_Y = 2 << 32

    def __init__(self, x1, y1, x2, y2):
        self.x1 = x1
        self.x2 = x2
        self.y1 = y1
        self.y2 = y2
        Line.max_X = max(Line.max_X, x1)
        Line.max_X = max(Line.max_X, x2)
        Line.min_X = min(Line.min_X, x1)
        Line.min_X = min(Line.min_X, x2)

        Line.max_Y = max(Line.max_Y, y1)
        Line.max_Y = max(Line.max_Y, y2)
        Line.min_Y = min(Line.min_Y, y1)
        Line.min_Y = min(Line.min_Y, y2)

    def draw(self, map):
        if self.x1 == self.x2:
            # vertical
            for n in range(abs(self.y1-self.y2)+1):
                map[self.y1+n][self.x1] = 255
        else:
            # horizontal
            for n in range(abs(self.x1-self.x2)+1):
                map[self.y1][self.x1+n] = 255

    def rebase(self):
        self.x1 -= Line.min_X-2
        self.x2 -= Line.min_X-2

    @staticmethod
    def _rebase():
        Line.max_X -= Line.min_X-2
        Line.min_X = 0


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def print_map(map):
    for m in map:
        print(m)


def main():
    lines = read_lines("Day_17/inputs/input1")
    if lines is None:
        return 1

    borders = []

    for line in lines:
        parts = line.split(' ')
        m = re.findall('([0-9]+)', parts[0])
        m2 = re.findall('([0-9]+)', parts[1])
        if line[0] == 'x':
            x1 = int(m[0])
            x2 = x1
            y1 = int(m2[0])
            y2 = int(m2[1])
        else:
            y1 = int(m[0])
            y2 = y1
            x1 = int(m2[0])
            x2 = int(m2[1])
        borders.append(Line(x1, y1, x2, y2))

    source = Source(500-Line.min_X+2, 0)

    for b in borders:
        b.rebase()

    Line._rebase()

    map = np.zeros([Line.max_Y+3, Line.max_X+3])

    running_water_color = 125
    still_water_color = 180
    # spring
    map[source.y][source.x] = running_water_color
    for b in borders:
        b.draw(map)

    ###############################################
    # Part One

    sources = [source]
    while len(sources) > 0:
        next = []
        for s in sources:
            if map[s.y][s.x] != running_water_color:
                continue
            end = False
            # drop down until you can
            while map[s.y+1][s.x] != 255 and map[s.y+1][s.x] != still_water_color:
                map[s.y][s.x] = running_water_color
                s.y += 1
                if s.y+1 > Line.max_Y:
                    map[s.y][s.x] = running_water_color
                    end = True
                    s.running = False
                    break
            if end:
                continue
            map[s.y][s.x] = running_water_color
            r = s.x+1
            l = s.x-1
            to_still = [(s.x, s.y)]
            sill_water = True
            # spread right until you can
            while map[s.y][r] != 255:
                if map[s.y+1][r] == 0:
                    map[s.y][r] = running_water_color
                    next.append(Source(r, s.y))
                    sill_water = False
                    break
                elif map[s.y+1][r] == running_water_color:
                    next.append(Source(r, s.y))
                    sill_water = False
                    break
                else:
                    map[s.y][r] = running_water_color
                    to_still.append((r, s.y))
                    r += 1
            # spread left until you can
            while map[s.y][l] != 255:
                if map[s.y+1][l] == 0:
                    map[s.y][l] = running_water_color
                    next.append(Source(l, s.y))
                    sill_water = False
                    break
                elif map[s.y+1][l] == running_water_color:
                    next.append(Source(l, s.y))
                    sill_water = False
                    break
                else:
                    map[s.y][l] = running_water_color
                    to_still.append((l, s.y))
                    l -= 1

            if sill_water:
                for l in to_still:
                    map[l[1]][l[0]] = still_water_color
                next.append(Source(s.x, s.y-1))
                map[s.y-1][s.x] = running_water_color
        sources = next

    sum_running = 0
    sum_still = 0
    for y in range(map.shape[0]):
        for x in range(map.shape[1]):
            if map[y][x] == running_water_color:
                sum_running += 1
            if map[y][x] == still_water_color:
                sum_still += 1

    # im = Image.fromarray(map)
    # im = im.convert('RGB')
    # im.save("your_file.bmp")

    print("Part 1: ", sum_still+sum_running-1)
    ###############################################
    # Part Two
    print("Part 2: ", sum_still)
    return 0


main()
