"""Day 04 problem solver"""
from collections import defaultdict


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


LOW = 264360
HIGH = 746325


def is_password(password):
    pass_str = str(password)
    prev_int = 0
    same = False
    for n in pass_str:
        v = int(n)
        if v < prev_int:
            return False
        else:
            if v == prev_int:
                same = True
        prev_int = v
    return same


def is_password2(password):
    pass_str = str(password)
    prev_int = 0
    numbers = defaultdict(int)
    for n in pass_str:
        v = int(n)
        if v < prev_int:
            return False
        else:
            numbers[n] += 1
        prev_int = v
    for n in numbers:
        if numbers[n] == 2:
            return True
    return False


def main():
    ###############################################
    # Part One
    passwords = range(LOW, HIGH)
    ret = len(list(filter(is_password, passwords)))
    print(f"Part 1: {ret}")
    ###############################################
    # Part Two
    ret = sum(list(filter(is_password2, passwords)))
    print(f"Part 2: {ret}")
    return 0


main()
