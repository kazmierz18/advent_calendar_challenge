"""Day 9 problem solver"""


from collections import deque


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def play_game(marble_max_score, players_num):
    players_score = [0 for i in range(players_num)]
    marbles = deque([0])
    current_marble_id = 0
    marbles.append(current_marble_id)
    current_player = 0
    for marble in range(marble_max_score+1):
        if marble % 23 == 0:
            marbles.rotate(7)
            players_score[current_player] += marble+marbles.pop()
            marbles.rotate(-1)
        else:
            marbles.rotate(-1)
            marbles.append(marble)
        current_player = (current_player+1) % players_num
    return max(players_score)


def main():
    lines = read_lines("Day_09/inputs/input1")
    if lines is None:
        return 1
    line = lines[0]
    parts = line.split(' ')
    players_num = int(parts[0])
    marble_max_score = int(parts[6])
    ###############################################
    # Part One
    print("Part 1: ", play_game(marble_max_score, players_num))
    ###############################################
    # Part Two
    print("Part 2: ", play_game(marble_max_score*100, players_num))
    return 0


main()
