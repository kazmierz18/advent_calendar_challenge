"""Day 05 problem solver"""
import copy
from intcode import intcode_cpu, preprocess

def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None

def main():
    lines = read_lines("Day_05/inputs/input1")
    if lines is None:
        return 1
    program=preprocess(lines)
    ###############################################
    # Part One
    cpu = intcode_cpu() 
    cpu.load_program(program)
    cpu.run()
    print("Part 1: " + cpu.output_stream)
    ###############################################
    # Part Two
    cpu2 = intcode_cpu()
    cpu2.load_program(program)
    cpu2.run()
    print("Part 2: " + cpu2.output_stream)
    return 0


main()
