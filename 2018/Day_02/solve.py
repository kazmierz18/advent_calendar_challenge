"""Day 2 problem solver"""


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def to_dictionary(line):
    "Changes line into a dictionary of letters -> occurrences"
    dictionary = {}
    for letter in line:
        if letter in dictionary:
            dictionary[letter] += 1
        else:
            dictionary[letter] = 1
    return dictionary


def main():
    lines = read_lines("Day_02/inputs/input1")
    if lines is None:
        return 1

    ###############################################
    # Prepare model
    dict_table = []
    for line in lines:
        dict_table.append(to_dictionary(line))
    
    ###############################################
    # Part One
    twos = 0
    threes = 0
    for element in dict_table:
        twice_seen = False
        three_seen = False
        for value in element.values():
            if value == 3 and not three_seen:
                threes += 1
                three_seen = True
            if value == 2 and not twice_seen:
                twice_seen = True
                twos += 1
    print("Part 1: ", twos, "*", threes, "=", twos*threes)

    ###############################################
    # Part Two
    print("Part 2: ", end='')
    for idx, line in enumerate(lines[0:-1]):
        for line2 in lines[(idx+1):]:
            count_different = 0
            for idx, letter in enumerate(line):
                if letter != line2[idx]:
                    count_different += 1
            if count_different == 1:
                for idx, letter in enumerate(line):
                    if letter == line2[idx]:
                        print(letter, end='')
                break
    return 0


main()
