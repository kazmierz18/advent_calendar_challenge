"""Day 21 problem solver"""

import re


class Disassembler:
    line = 0

    def __init__(self, pc_id):
        Disassembler.pc_id = pc_id
        Disassembler.op_codes = {'addr': Disassembler.addr,
                                 'addi': Disassembler.addi,
                                 'mulr': Disassembler.mulr,
                                 'muli': Disassembler.muli,
                                 'banr': Disassembler.banr,
                                 'bani': Disassembler.bani,
                                 'borr': Disassembler.borr,
                                 'bori': Disassembler.bori,
                                 'setr': Disassembler.setr,
                                 'seti': Disassembler.seti,
                                 'gtir': Disassembler.gtir,
                                 'gtri': Disassembler.gtri,
                                 'gtrr': Disassembler.gtrr,
                                 'eqir': Disassembler.eqir,
                                 'eqri': Disassembler.eqri,
                                 'eqrr': Disassembler.eqrr,
                                 'divi': Disassembler.divi,
                                 'noop': Disassembler.noop,
                                 }

    @staticmethod
    def translate(opcode, A, B, C):
        print('{:02d}'.format(Disassembler.line), ': ', end='', sep='')
        Disassembler.op_codes[opcode](A, B, C)
        Disassembler.line += 1

    #  mnemonics
    @staticmethod
    def addr(A, B, C):
        if C == Disassembler.pc_id:
            print('goto +r[', A, '];', sep='')
        else:
            print('r[', C, '] = r[', A, '] + r[', B, '];', sep='')

    @staticmethod
    def addi(A, B, C):
        if C == Disassembler.pc_id:
            print('goto +', B, ';', sep='')
        else:
            print('r[', C, '] = r[', A, '] + ', B, ';', sep='')

    @staticmethod
    def mulr(A, B, C):
        print('r[', C, '] = r[', A, '] * r[', B, '];', sep='')

    @staticmethod
    def muli(A, B, C):
        print('r[', C, '] = r[', A, '] * ', B, ';', sep='')

    @staticmethod
    def banr(A, B, C):
        print('r[', C, '] = r[', A, '] & r[', B, '];', sep='')

    @staticmethod
    def bani(A, B, C):
        print('r[', C, '] = r[', A, '] & ', '0x{:02x}'.format(B), ';', sep='')

    @staticmethod
    def borr(A, B, C):
        print('r[', C, '] = r[', A, '] | r[', B, '];', sep='')

    @staticmethod
    def bori(A, B, C):
        print('r[', C, '] = r[', A, '] | ', '0x{:02x}'.format(B), ';', sep='')

    @staticmethod
    def setr(A, B, C):
        print('r[', C, '] = r[', A, '];', sep='')

    @staticmethod
    def seti(A, B, C):
        if C == Disassembler.pc_id:
            print('goto ', '{:02d}'.format(A+1), ';', sep='')
        else:
            print('r[', C, '] = ', A, ';', sep='')

    @staticmethod
    def gtir(A, B, C):
        print('r[', C, '] = (', A, '>r[', B, '])?1:0;', sep='')

    @staticmethod
    def gtri(A, B, C):
        print('r[', C, '] = (r[', A, ']>', B, ')?1:0;', sep='')

    @staticmethod
    def gtrr(A, B, C):
        print('r[', C, '] = (r[', A, ']>r[', B, '])?1:0;', sep='')

    @staticmethod
    def eqir(A, B, C):
        print('r[', C, '] = (', A, '==r[', B, '])?1:0;', sep='')

    @staticmethod
    def eqri(A, B, C):
        print('r[', C, '] = (r[', A, ']==', B, ')?1:0;', sep='')

    @staticmethod
    def eqrr(A, B, C):
        print('r[', C, '] = (r[', A, ']==r[', B, '])?1:0;', sep='')

    @staticmethod
    def divi(A, B, C):
        print('r[', C, '] = r[', A, '] / ', B, '', sep='')

    @staticmethod
    def noop(A, B, C):
        print()


class CPU:
    registers = [0]*6
    pc_id = 0

    def __init__(self, pc_id):
        CPU.pc_id = pc_id
        CPU.op_codes = {'addr': CPU.addr,
                        'addi': CPU.addi,
                        'mulr': CPU.mulr,
                        'muli': CPU.muli,
                        'banr': CPU.banr,
                        'bani': CPU.bani,
                        'borr': CPU.borr,
                        'bori': CPU.bori,
                        'setr': CPU.setr,
                        'seti': CPU.seti,
                        'gtir': CPU.gtir,
                        'gtri': CPU.gtri,
                        'gtrr': CPU.gtrr,
                        'eqir': CPU.eqir,
                        'eqri': CPU.eqri,
                        'eqrr': CPU.eqrr,
                        'divi': CPU.divi,
                        'noop': CPU.noop,
                        }

    @staticmethod
    def set_registers(A, B, C, D, E, F):
        CPU.registers[0] = A
        CPU.registers[1] = B
        CPU.registers[2] = C
        CPU.registers[3] = D
        CPU.registers[4] = E
        CPU.registers[5] = F

    @staticmethod
    def compare_register(A, B, C, D):
        return CPU.registers[0] == A and \
            CPU.registers[1] == B and \
            CPU.registers[2] == C and \
            CPU.registers[3] == D

    @staticmethod
    def execute(opcode, A, B, C):
        CPU.op_codes[opcode](A, B, C)

    #  mnemonics
    @staticmethod
    def addr(A, B, C):
        CPU.registers[C] = CPU.registers[A]+CPU.registers[B]

    @staticmethod
    def addi(A, B, C):
        CPU.registers[C] = CPU.registers[A]+B

    @staticmethod
    def mulr(A, B, C):
        CPU.registers[C] = CPU.registers[A]*CPU.registers[B]

    @staticmethod
    def muli(A, B, C):
        CPU.registers[C] = CPU.registers[A]*B

    @staticmethod
    def banr(A, B, C):
        CPU.registers[C] = CPU.registers[A] & CPU.registers[B]

    @staticmethod
    def bani(A, B, C):
        CPU.registers[C] = CPU.registers[A] & B

    @staticmethod
    def borr(A, B, C):
        CPU.registers[C] = CPU.registers[A] | CPU.registers[B]

    @staticmethod
    def bori(A, B, C):
        CPU.registers[C] = CPU.registers[A] | B

    @staticmethod
    def setr(A, B, C):
        CPU.registers[C] = CPU.registers[A]

    @staticmethod
    def seti(A, B, C):
        CPU.registers[C] = A

    @staticmethod
    def gtir(A, B, C):
        CPU.registers[C] = 1 if A > CPU.registers[B] else 0

    @staticmethod
    def gtri(A, B, C):
        CPU.registers[C] = 1 if CPU.registers[A] > B else 0

    @staticmethod
    def gtrr(A, B, C):
        CPU.registers[C] = 1 if CPU.registers[A] > CPU.registers[B] else 0

    @staticmethod
    def eqir(A, B, C):
        CPU.registers[C] = 1 if A == CPU.registers[B] else 0

    @staticmethod
    def eqri(A, B, C):
        CPU.registers[C] = 1 if CPU.registers[A] == B else 0

    @staticmethod
    def eqrr(A, B, C):
        CPU.registers[C] = 1 if CPU.registers[A] == CPU.registers[B] else 0

    @staticmethod
    def divi(A, B, C):
        CPU.registers[C] = CPU.registers[A] // B

    @staticmethod
    def noop(A, B, C):
        pass


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def main():
    lines = read_lines("Day_21/inputs/input2")
    if lines is None:
        return 1
    m = re.findall('([0-9]+)', lines[0])
    del lines[0]
    CPU(int(m[0]))
    instructions = []
    for line in lines:
        parts = line.split(' ')
        instructions.append(
            (parts[0], int(parts[1]), int(parts[2]), int(parts[3])))
    # Disassembler(int(m[0]))
    # for i in instructions:
    #     Disassembler.translate(i[0], i[1], i[2], i[3])
    ###############################################
    # Part One

    values = []
    while CPU.registers[CPU.pc_id] < len(lines):
        if CPU.registers[CPU.pc_id] == 28:
            if CPU.registers[1] in values:
                break
            values.append(CPU.registers[1])
            # print(CPU.registers[1])
        i = instructions[CPU.registers[CPU.pc_id]]
        CPU.execute(i[0], i[1], i[2], i[3])
        CPU.registers[CPU.pc_id] += 1
    print("Part 1: ", values[0])
    ###############################################
    # Part Two
    print("Part 2: ", values[-1])
    return 0


main()

# 00: r[1] = 123;
# 01: r[1] = r[1] & 0x1c8;
# 02: r[1] = (r[1]==72)?1:0;
# 03: goto +r[1];
# 04: goto 01;
# 05: r[1] = 0;
# 06: r[3] = r[1] | 0x10000;
# 07: r[1] = 6780005;
# 08: r[2] = r[3] & 0xff;
# 09: r[1] = r[1] + r[2];
# 10: r[1] = r[1] & 0xffffff;
# 11: r[1] = r[1] * 65899;
# 12: r[1] = r[1] & 0xffffff;
# 13: r[2] = (256>r[3])?1:0;
# 14: goto +r[2];
# 15: goto +1;
# 16: goto 28;
# 17: r[2] = 0;
# 18: r[5] = r[2] + 1;              addi 3 -256 3
# 19: r[5] = r[5] * 256;            divi 3 256 3
# 20: r[5] = (r[5]>r[3])?1:0;       addi 3 1 3
# 21: goto +r[5];                   noop
# 22: goto +1;                      noop
# 23: goto 26;                      noop
# 24: r[2] = r[2] + 1;              noop
# 25: goto 18;                      noop
# 26: r[3] = r[2];
# 27: goto 08;
# 28: r[2] = (r[1]==r[0])?1:0;
# 29: goto +r[2];
# 30: goto 06;
