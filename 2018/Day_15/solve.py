"""Day 15 problem solver"""

import copy

NEAREST_POINTS =[(-1,0),(1,0),(0,-1),(0,1)]

class Mob:
    elf_attack = 3

    def __init__(self, type, x, y):
        self.x = x
        self.y = y
        self.type = type
        self.health = 200
        self.fighting = False

    def NY_distance(self, other):
        return abs(self.x-other.x)+abs(self.y-other.y)

    def fight(self, other, map):
        if self.type == 'E':
            self.attack = Mob.elf_attack
        else:
            self.attack = 3
        other.health -= self.attack
        if not other.is_alive():
            map[other.y][other.x] = '.'

    def is_alive(self):
        return True if self.health > 0 else False

    def is_fighting(self):
        return self.fighting


def move(mob, target, map, original):
    count = 2 << 32
    current = [(target.x-1, target.y), (target.x+1, target.y),
               (target.x, target.y-1), (target.x, target.y+1)]
    moves = []
    while count > 1:
        next = []
        for c in current:
            if map[c[1]][c[0]].isdigit():
                l = int(map[c[1]][c[0]])
                if l == 1:
                    moves.append(c)
                if l <= count:
                    next += [(c[0]-1, c[1]), (c[0]+1, c[1]),
                             (c[0], c[1]-1), (c[0], c[1]+1)]
                    count = l
            current = next
    moves.sort(key=lambda m: (m[1], m[0]))
    original[mob.y][mob.x] = '.'
    mob.x = moves[0][0]
    mob.y = moves[0][1]
    original[mob.y][mob.x] = mob.type


# this is the trickiest part of simulation
# we need to find the nearest target considering the obstacles
def find_nearest(mob, mobs, map):
    enemy_type = 'G' if mob.type == 'E' else 'E'
    targets = []
    map2 = copy.deepcopy(map)
    current = [(mob.x-1, mob.y), (mob.x+1, mob.y),
               (mob.x, mob.y-1), (mob.x, mob.y+1)]
    count = 0
    while len(current) > 0:
        count += 1
        next = []
        for c in current:
            if map2[c[1]][c[0]] == '.':
                map2[c[1]][c[0]] = str(count)
                next += [(c[0]-1, c[1]), (c[0]+1, c[1]),
                         (c[0], c[1]-1), (c[0], c[1]+1)]
            elif map2[c[1]][c[0]] == enemy_type:
                targets.append((c[0], c[1]))
        if len(targets) > 0:
            break
        current = next
    # print(targets)
    targets.sort(key=lambda t: (t[1], t[0]))
    if len(targets) == 0:
        # there is no targets
        return
    t = targets[0]
    target = None
    for m in mobs:
        if m.x == t[0] and m.y == t[1]:
            target = m
    if target == None:
        return
    # target=next(m for m in mobs if m.x==t[0] and m.y==t[1])
    if mob.NY_distance(target) == 1:
        mob.fighting = True
        fight(mob, mobs, map)
    else:
        # move to target
        move(mob, target, map2, map)
        if mob.NY_distance(target) == 1:
            fight(mob, mobs, map)
    # print_battelfield(map)


def fight(mob, mobs, map):
    enemies = []
    for enemy in mobs:
        # find all mobs in distance of 1 (can attack)
        if mob.type != enemy.type and enemy.is_alive() and mob.NY_distance(enemy) == 1:
            enemies.append(enemy)
    if len(enemies) >= 1:
        # if there are mobs, find one with lowest HP and kill it
        enemies.sort(key=lambda x: x.health)
        mob.fight(enemies[0], map)
    else:
        mob.fighting = False
        find_nearest(mob, mobs, map)


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def print_battelfield(field):
    for battle_line in field:
        print(''.join(battle_line))


def play_round(mobs, map):
    idx = 0
    while idx < len(mobs):
        if mobs[idx].is_alive():
            if mobs[idx].is_fighting():
                # if mob was fighting in previous round, keep fighting
                fight(mobs[idx], mobs, map)
            else:
                # otherwise look for new target
                find_nearest(mobs[idx], mobs, map)
        idx += 1


def game_over(mobs):
    v = 0
    for m in mobs:
        if m.type == 'E':
            v |= 1
        else:
            v |= 2
        if v == 3:
            return False
    return True


def elf_died(mobs):
    for m in mobs:
        if m.type == 'E' and not m.is_alive():
            return True
    return False


def main():
    lines = read_lines("Day_15/inputs/input1")
    if lines is None:
        return 1
    mobs = []
    map = [['' for x in range(len(lines[0]))]
           for y in range(len(lines))]
    for y, line in enumerate(lines):
        for x, l in enumerate(line):
            if l == 'G' or l == 'E':
                mobs.append(Mob(l, x, y))
            map[y][x] = l
    map_2 = copy.deepcopy(map)
    mobs_2 = copy.deepcopy(mobs)
    # print_battelfield(map)
    ###############################################
    # Part One
    round = 1
    while True:
        play_round(mobs, map)
        mobs = [m for m in mobs if m.is_alive()]
        # print('Round: ', round)
        round += 1
        if game_over(mobs):
            break
        mobs.sort(key=lambda m: (m.y, m.x))

    print("Part 1: ", sum(x.health for x in mobs)*(round-1))
    ###############################################
    # Part Two
    elves_victory = False
    Mob.elf_attack += 1
    while not elves_victory:
        mobs = copy.deepcopy(mobs_2)
        map = copy.deepcopy(map_2)
        round = 1
        while True:
            play_round(mobs, map)
            if elf_died(mobs):
                break
            mobs = [m for m in mobs if m.is_alive()]
            # print('Round: ', round)
            if game_over(mobs):
                elves_victory = True
                break
            round += 1
            mobs.sort(key=lambda m: (m.y, m.x))
        Mob.elf_attack += 1
    # print_battelfield(map)
    # for m in mobs:
    #     print(m.type, m.health)
    print("Part 2: ", sum(x.health for x in mobs)*(round-1))
    return 0


main()
