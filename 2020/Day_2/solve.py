"""Day 2 problem solver"""


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


class Rule:
    def __init__(self, r):
        numbers, c = r.split(' ')
        self.c = c
        l, m = numbers.split('-')
        self.l = int(l)
        self.m = int(m)

    def checkPolicy(self, password):
        cnt = 0
        for c in password:
            if c == self.c:
                cnt += 1
        return cnt >= self.l and cnt <= self.m

    def checkPolicy2(self, password):
        return bool(password[self.l-1] == self.c) ^ bool(password[self.m-1] == self.c)


class dbEntry:
    def __init__(self, rule, password):
        self.rule = Rule(rule)
        self.password = password

    def checkPolicy(self):
        return self.rule.checkPolicy(self.password)

    def checkPolicy2(self):
        return self.rule.checkPolicy2(self.password)


def main():
    lines = read_lines("Day_2/inputs/input1")
    if lines is None:
        return 1
    ###############################################
    # Part One
    database = []
    for line in lines:
        rule, password = line.split(':')
        database.append(dbEntry(rule, password.strip()))

    cnt = 0
    for e in database:
        if e.checkPolicy():
            cnt += 1

    print("Part 1: ")
    print(cnt)
    ###############################################
    # Part Two
    cnt = 0
    for e in database:
        if e.checkPolicy2():
            cnt += 1

    print("Part 2: ")
    print(cnt)
    return 0


main()
