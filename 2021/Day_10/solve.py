"""Day 10 problem solver"""
from pprint import pprint
from collections import deque


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def close_bracket(cc):
    if cc == '(':
        return ')'
    elif cc == '[':
        return ']'
    elif cc == '{':
        return '}'
    elif cc == '<':
        return '>'
    return ''


def main():
    lines = read_lines("Day_10/inputs/input1")
    if lines is None:
        return 1
    ###############################################
    # Part One
    score = {')': 3, ']': 57, '}': 1197, '>': 25137}
    ret = 0
    correct = []
    for line in lines:
        stack = deque()
        incorrect = False
        for c in line:
            if c == '(' or c == '[' or c == '{' or c == '<':
                stack.append(c)
            if c == ')' or c == ']' or c == '}' or c == '>':
                cc = close_bracket(stack.pop())
                if cc != c:
                    print(f'incorrect line expected {cc} but found {c}')
                    ret += score[c]
                    incorrect = True
                    break
        if not incorrect:
            correct.append(line)

    print(f"Part 1: {ret}")
    ###############################################
    # Part Two
    score = {')': 1, ']': 2, '}': 3, '>': 4}
    rr= []
    for line in correct:
        stack = deque()
        for c in line:
            if c == '(' or c == '[' or c == '{' or c == '<':
                stack.append(c)
            if c == ')' or c == ']' or c == '}' or c == '>':
                cc = stack.pop()
        closing = list(reversed([close_bracket(cc) for cc in stack]))
        r = 0
        for c in closing:
            r = r * 5 + score[c]
        rr.append(r)
    rr.sort()
    ret = rr[len(rr)//2]

    print(f"Part 2: {ret}")
    return 0


main()
