"""Day 19 problem solver"""

import re


class CPU:
    registers = [0]*6
    pc_id = 0

    def __init__(self, pc_id):
        CPU.pc_id = pc_id
        CPU.op_codes = {'addr': CPU.addr,
                        'addi': CPU.addi,
                        'mulr': CPU.mulr,
                        'muli': CPU.muli,
                        'banr': CPU.banr,
                        'bani': CPU.bani,
                        'borr': CPU.borr,
                        'bori': CPU.bori,
                        'setr': CPU.setr,
                        'seti': CPU.seti,
                        'gtir': CPU.gtir,
                        'gtri': CPU.gtri,
                        'gtrr': CPU.gtrr,
                        'eqir': CPU.eqir,
                        'eqri': CPU.eqri,
                        'eqrr': CPU.eqrr
                        }

    @staticmethod
    def set_registers(A, B, C, D, E, F):
        CPU.registers[0] = A
        CPU.registers[1] = B
        CPU.registers[2] = C
        CPU.registers[3] = D
        CPU.registers[4] = E
        CPU.registers[5] = F

    @staticmethod
    def compare_register(A, B, C, D):
        return CPU.registers[0] == A and \
            CPU.registers[1] == B and \
            CPU.registers[2] == C and \
            CPU.registers[3] == D

    @staticmethod
    def execute(opcode, A, B, C):
        # try:
        CPU.op_codes[opcode](A, B, C)
        # except:
        #     pass

    #  mnemonics
    @staticmethod
    def addr(A, B, C):
        CPU.registers[C] = CPU.registers[A]+CPU.registers[B]

    @staticmethod
    def addi(A, B, C):
        CPU.registers[C] = CPU.registers[A]+B

    @staticmethod
    def mulr(A, B, C):
        CPU.registers[C] = CPU.registers[A]*CPU.registers[B]

    @staticmethod
    def muli(A, B, C):
        CPU.registers[C] = CPU.registers[A]*B

    @staticmethod
    def banr(A, B, C):
        CPU.registers[C] = CPU.registers[A] & CPU.registers[B]

    @staticmethod
    def bani(A, B, C):
        CPU.registers[C] = CPU.registers[A] & B

    @staticmethod
    def borr(A, B, C):
        CPU.registers[C] = CPU.registers[A] | CPU.registers[B]

    @staticmethod
    def bori(A, B, C):
        CPU.registers[C] = CPU.registers[A] | B

    @staticmethod
    def setr(A, B, C):
        CPU.registers[C] = CPU.registers[A]

    @staticmethod
    def seti(A, B, C):
        CPU.registers[C] = A

    @staticmethod
    def gtir(A, B, C):
        CPU.registers[C] = 1 if A > CPU.registers[B] else 0

    @staticmethod
    def gtri(A, B, C):
        CPU.registers[C] = 1 if CPU.registers[A] > B else 0

    @staticmethod
    def gtrr(A, B, C):
        CPU.registers[C] = 1 if CPU.registers[A] > CPU.registers[B] else 0

    @staticmethod
    def eqir(A, B, C):
        CPU.registers[C] = 1 if A == CPU.registers[B] else 0

    @staticmethod
    def eqri(A, B, C):
        CPU.registers[C] = 1 if CPU.registers[A] == B else 0

    @staticmethod
    def eqrr(A, B, C):
        CPU.registers[C] = 1 if CPU.registers[A] == CPU.registers[B] else 0


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def main():
    lines = read_lines("Day_19/inputs/input1")
    if lines is None:
        return 1
    # print(lines)
    m = re.findall('([0-9]+)', lines[0])
    del lines[0]
    CPU(int(m[0]))

    ###############################################
    # Part One
    while CPU.registers[CPU.pc_id]<len(lines):
        parts=lines[CPU.registers[CPU.pc_id]].split(' ')
        CPU.execute(parts[0], int(parts[1]), int(parts[2]), int(parts[3]))
        CPU.registers[CPU.pc_id]+=1
    print("Part 1: ", CPU.registers[0])
    ###############################################
    # Part Two
    CPU.set_registers(1, 0, 0, 0, 0, 0)
    while CPU.registers[CPU.pc_id] < len(lines):
        parts = lines[CPU.registers[CPU.pc_id]].split(' ')
        # print(CPU.registers[CPU.pc_id], parts[0], parts[1], parts[2], parts[3])
        CPU.execute(parts[0], int(parts[1]), int(parts[2]), int(parts[3]))
        CPU.registers[CPU.pc_id] += 1
        if CPU.registers[CPU.pc_id]==2:
            break
    print(CPU.registers[4])
    s=sum([x for x in range(1, CPU.registers[4]+1) if CPU.registers[4] % x == 0])
    CPU.registers[0]=0
    print("Part 2: ", s)
    return 0


main()






































# addi 3 16 3   GOTO 17
# seti 1 3 2    r2=1
# seti 1 0 5    r5=1
# mulr 2 5 1    r1=r2*r5
# eqrr 1 4 1    cmp r1 r4
# addr 1 3 3    if GOTO 7
# addi 3 1 3    else GOTO 8
# addr 2 0 0    r0+=r2
# addi 5 1 5    r5+=1
# gtrr 5 4 1    cmp r5>r4
# addr 3 1 3    if GOTO 12
# seti 2 2 3    else GOTO 3
# addi 2 1 2    r2+=1
# gtrr 2 4 1    cmp r2>r4
# addr 1 3 3    if GOTO 16
# seti 1 1 3    else GOTO 2
# mulr 3 3 3    pc*=pc -> this seems to break the loop cause 16*16 will go outside program
# addi 4 2 4    r4=2
# mulr 4 4 4    r4*=r4
# mulr 3 4 4    r4=pc*r4
# muli 4 11 4   r4*=11
# addi 1 4 1    r1=4
# mulr 1 3 1    r1=pc*r1
# addi 1 2 1    r1+=2
# addr 4 1 4    r4+=r1
# addr 3 0 3    GOTO R0 First problem 26, Second problem 27
# seti 0 2 3    GOTO 0
# setr 3 6 1    r1=pc
# mulr 1 3 1    r1*=pc
# addr 3 1 1    r1+=pc
# mulr 3 1 1    r1*=pc
# muli 1 14 1   r1*=14
# mulr 1 3 1    r1*=pc
# addr 4 1 4    r4+=r1
# seti 0 6 0    r0=0
# seti 0 9 3    GOTO 1

# r2=1
# #######[0, 10550400, 1, 2, 10551326, 0]
# do{
#   r5=1
#   r1=r2*r5
#   if(r1==r4){
#       r0+=r2
#   }
#   r5+=1
#   if(r4<=r5){
#       continue;
#   }else{
#       r2+=1
#   }
# }while(r2>r4)


# 00: goto +16;
# 01: r[2] = 1;
# 02: r[5] = 1;
# 03: r[1] = r[2] * r[5];
# 04: r[1] = (r[1]==r[4])?1:0;
# 05: goto +r[1];
# 06: goto +1;
# 07: r[0] = r[2] + r[0];
# 08: r[5] = r[5] + 1;
# 09: r[1] = (r[5]>r[4])?1:0;
# 10: goto +r[3];
# 11: goto 03;
# 12: r[2] = r[2] + 1;
# 13: r[1] = (r[2]>r[4])?1:0;
# 14: goto +r[1];
# 15: goto 02;
# 16: r[3] = r[3] * r[3];
# 17: r[4] = r[4] + 2;
# 18: r[4] = r[4] * r[4];
# 19: r[4] = r[3] * r[4];
# 20: r[4] = r[4] * 11;
# 21: r[1] = r[1] + 4;
# 22: r[1] = r[1] * r[3];
# 23: r[1] = r[1] + 2;
# 24: r[4] = r[4] + r[1];
# 25: goto +r[3];
# 26: goto 01;
# 27: r[1] = r[3];
# 28: r[1] = r[1] * r[3];
# 29: r[1] = r[3] + r[1];
# 30: r[1] = r[3] * r[1];
# 31: r[1] = r[1] * 14;
# 32: r[1] = r[1] * r[3];
# 33: r[4] = r[4] + r[1];
# 34: r[0] = 0;
# 35: goto 01;



