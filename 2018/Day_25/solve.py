"""Day 25 problem solver"""

import re
from dataclasses import dataclass


@dataclass
class Point:
    x: int
    y: int
    z: int
    w: int

    def distance(self, other):
        dx = abs(self.x-other.x)
        dy = abs(self.y-other.y)
        dz = abs(self.z-other.z)
        dw = abs(self.w-other.w)
        return dx+dy+dz+dw


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def main():
    lines = read_lines("Day_25/inputs/input1")
    if lines is None:
        return 1

    points = list(map(lambda l: Point(*map(int,(l.split(',')))), lines))
    points.sort(key=lambda p: p.x+p.y+p.z+p.w)

    ###############################################
    # Part One
    constellations = [[points.pop(0)]]
    i = 0
    while points:
        added = False
        cs = constellations[-1]
        for c in cs:
            i = 0
            while i < len(points):
                if c.distance(points[i]) <= 3:
                    cs.append(points.pop(i))
                    added = True
                else:
                    i += 1
        if not added:
            constellations.append([points.pop(0)])

    print("Part 1: ", len(constellations))
    ###############################################
    # Part Two
    print("Part 2: ")
    return 0


main()
