"""Day  problem solver"""


class Place:
    highest_x = 0
    highest_y = 0

    def __init__(self, x, y):
        self.x = x
        Place.highest_x = max(Place.highest_x, x)
        self.y = y
        Place.highest_y = max(Place.highest_y, y)

    def distance(self, x, y):
        return abs(self.x-x)+abs(self.y-y)


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def main():
    lines = read_lines("Day_06/inputs/input1")
    if lines is None:
        return 1

    places = []
    for idx, line in enumerate(lines):
        parts = line.split(', ')
        places.append(Place(int(parts[0]), int(parts[1])))

    ##########################################
    # Part One
    area = [[-1 for i in range(Place.highest_x+1)]
            for i in range(Place.highest_y+1)]

    for idx, place in enumerate(places):
        area[place.y][place.x] = idx

    for y in range(len(area)):
        for x in range(len(area[0])):
            smallest = 2 << 32
            history = []
            for idx, p in enumerate(places):
                dist = p.distance(x, y)
                if dist <= smallest:
                    smallest = dist
                    area[y][x]=idx
                    history.append((idx, dist))
            if len(history)>1:
                if history[-1][1] ==history[-2][1]:
                    area[y][x]=''

    v=[0]*len(places)
    for n in range(len(places)):
        for y in range(len(area)):
            for x in range(len(area[0])):
                if (y==0 or x==0 or y==len(area)-1 or x==len(area[0])) and area[y][x]==n:
                    v[n]=-1
                    break
                elif area[y][x]==n:
                    v[n]+=1
            if v[n]==-1:
                break
    print('Part 1: ', max(v))

    ##########################################
    #Part Two
    area = [[0 for i in range(Place.highest_x+1)]
        for i in range(Place.highest_y+1)]

    count=0
    for y in range(len(area)):
        for x in range(len(area[0])):
            total = 0
            for idx, p in enumerate(places):
                total += p.distance(x, y)
            if total<10000:
                count+=1

    print('Part 2: ', count)
    return 0

main()
