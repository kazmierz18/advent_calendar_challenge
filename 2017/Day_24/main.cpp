/*===============*/
/* Day 24        */
/*===============*/
#include <algorithm>
#include <fstream>
#include <iostream>
#include <list>
#include <memory>
#include <string>
#include <vector>

struct connection {
  int a;
  int b;
  int sum;
  std::vector<std::shared_ptr<connection>> nexts;
  connection(const int a, const int b) : a(a), b(b), sum(a + b) {}
  void create_connections(
      std::vector<std::shared_ptr<connection>> const &connections,
      const int level) {
    for (auto i = 0; i < level; i++) {
      std::cout << "\t";
    }
    std::cout << "(" << a << "," << b << ")"
              << "\n";
    auto found = connections.begin();
    auto pred = [this](std::shared_ptr<connection> const &obj) {
      return (obj->a == b or obj->b == b);
    };
    while ((found = std::find_if(found, connections.end(), pred)) !=
           connections.end()) {
      auto cpy_found = std::shared_ptr<connection>(
          new connection(b, (*found)->a != b ? (*found)->a : (*found)->b));
      nexts.push_back(cpy_found);
      std::vector<std::shared_ptr<connection>> left;
      left.reserve(connections.size() - 1);
      std::remove_copy(connections.begin(), connections.end(),
                       std::back_inserter(left), *found);
      cpy_found->create_connections(left, level + 1);
      found++;
    }
  }

  int count_weight(const int total) const {
    if (nexts.empty()) {
      return total + sum;
    }
    auto max = 0;
    for (auto const next : nexts) {
      auto const tmp = next->count_weight(total + sum);
      if (tmp > max) {
        max = tmp;
      }
    }
    return max;
  }

  std::tuple<int, int> count_longest(int const total, int const level) const {
    if (nexts.empty()) {
      return {level, total + sum};
    }
    auto max_weight = 0;
    auto max_len = 0;
    for (auto const next : nexts) {
      auto const [len, weight] = next->count_longest(total + sum, level + 1);
      if (len >= max_len) {
        max_len = len;
        if (weight > max_weight) {
          max_weight = weight;
        }
      }
    }
    return {max_len, max_weight};
  }
};

auto split(const std::string &line, const char *delimiter) {
  std::vector<std::string> splitted;
  auto currentIdx = -1;
  do {
    currentIdx++;
    const auto foundIdx = line.find_first_of(delimiter, currentIdx);
    splitted.push_back(line.substr(currentIdx, foundIdx - currentIdx));
    currentIdx = foundIdx;
  } while (currentIdx != std::string::npos);
  return splitted;
}

int main(int argc, char **argv) {
  // read input file
  char *name = nullptr;
  if (argc >= 2) {
    name = argv[1];
  } else {
    return 1;
  }
  std::ifstream file(name);
  std::string tmp;
  std::vector<std::string> lines;
  while (std::getline(file, tmp)) {
    // remove new line characters
    if (*(tmp.end()) == '\n') {
      tmp.pop_back();
    }
    lines.push_back(tmp);
  }
  std::vector<std::shared_ptr<connection>> connections;
  // connections.reserve(lines.size);
  for (auto const line : lines) {
    auto const splitted = split(line, "/");
    auto const a = std::atoi(splitted[0].c_str());
    auto const b = std::atoi(splitted[1].c_str());
    connections.push_back(std::shared_ptr<connection>(
        new connection(std::min(a, b), std::max(a, b))));
  }
  std::sort(connections.begin(), connections.end(),
            [](std::shared_ptr<connection> &lhs,
               const std::shared_ptr<connection> &rhs) {
              return lhs->sum > rhs->sum;
            });
  auto pred = [](std::shared_ptr<connection> const &obj) {
    return obj->a != 0;
  };
  std::vector<std::shared_ptr<connection>> starts;
  std::remove_copy_if(connections.begin(), connections.end(),
                      std::back_inserter(starts), pred);
  auto pred2 = [](std::shared_ptr<connection> const &obj) {
    return obj->a == 0;
  };
  std::vector<std::shared_ptr<connection>> left;
  left.reserve(connections.size() - starts.size());
  std::remove_copy_if(connections.begin(), connections.end(),
                      std::back_inserter(left), pred2);
  auto max = 0;
  for (auto const start : starts) {
    start->create_connections(left, 0);
    auto const sum = start->count_weight(0);
    if (sum > max) {
      max = sum;
    }
  }
  //================================================
  std::cout << "Part One: " << max << "\n";
  //================================================
  auto max_len = 0;
  auto max_weight = 0;
  for (auto const start : starts) {
    start->create_connections(left, 0);
    auto const [sum_len, sum_weight] = start->count_longest(0, 0);
    if (sum_len >= max_len) {
      max_len = sum_len;
      if (sum_weight > max_weight) {
        max_weight = sum_weight;
      }
    }
  }
  std::cout << "Part Two: " << max_weight << "\n";
  return 0;
}
