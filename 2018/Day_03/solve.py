"""Day 3 problem solver"""

import re


class PieceOfMaterial:
    highest_X = 0
    highest_Y = 0

    def __init__(self, id, x, y, dx, dy):
        self.id = id
        self.x = x
        self.y = y
        self.width = dx
        self.height = dy
        PieceOfMaterial.highest_X = max(
            PieceOfMaterial.highest_X, self.x+self.width)
        PieceOfMaterial.highest_Y = max(
            PieceOfMaterial.highest_Y, self.y+self.height)

    def cut_from_material(self, material):
        count = 0
        for py in range(self.height):
            for px in range(self.width):
                if material[self.x+px][self.y+py] != 0:
                    if material[self.x+px][self.y+py] != -1:
                        count += 1
                    material[self.x+px][self.y+py] = -1
                else:
                    material[self.x+px][self.y+py] = self.id
        return (material, count)

    def is_intact(self, material):
        for py in range(self.height):
            for px in range(self.width):
                if material[self.x+px][self.y+py] != self.id:
                    return False
        return True


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def parse_pieces(lines):
    "parse line to piece: #1 @ 1,3: 4x4"
    pieces = []
    for line in lines:
        m = re.findall('(\d+)', line)
        pieces.append(PieceOfMaterial(int(m[0]), int(
            m[1]), int(m[2]), int(m[3]), int(m[4])))
    return pieces


def main():
    lines = read_lines("Day_03/inputs/input1")
    if lines is None:
        return 1

    #################################################################
    # Prepare model
    pieces = parse_pieces(lines)
    material = [[0 for i in range(PieceOfMaterial.highest_X)]
                for i in range(PieceOfMaterial.highest_Y)]
    common_pieces_cnt = 0

    #################################################################
    # Part One
    for piece in pieces:
        material, count = piece.cut_from_material(material)
        common_pieces_cnt += count
    print("Part 1: ", common_pieces_cnt)

    #################################################################
    # Part Two
    id = -1
    for piece in pieces:
        if piece.is_intact(material):
            id = piece.id
            break
    print("Part 2: ", id)
    return 0


main()
