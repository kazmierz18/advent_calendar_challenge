#!/bin/bash

if [ $# -ne 1 ]; then
    echo "Specify day number"
    exit 1
fi
DAY_NUM=$1
DAY_FOLDER_NAME="Day_$DAY_NUM"
if [ ! -d "$DAY_FOLDER_NAME" ]; then
    echo "Creating dir for day $DAY_NUM"
    mkdir "$DAY_FOLDER_NAME"

    mkdir "$DAY_FOLDER_NAME/inputs"
    touch "$DAY_FOLDER_NAME/inputs/input1"
    touch "$DAY_FOLDER_NAME/inputs/input2"
    touch "$DAY_FOLDER_NAME/Challenge.md"

    SOLVE_FILE="$DAY_FOLDER_NAME/solve.py"
    
    {
        echo "\"\"\"Day $DAY_NUM problem solver\"\"\"" ;
        echo "";
        echo "";
        echo "def read_lines(filename):";
        echo "    \"Read input data\"";
        echo "    with open(filename) as file:";
        echo "        lines = file.readlines()";
        echo "        return [l.rstrip('\r\n') for l in lines]";
        echo "    return None";
        echo "";
        echo "";
        echo "def main():";
        echo "    lines = read_lines(\"Day_$DAY_NUM/inputs/input2\")";
        echo "    if lines is None:";
        echo "        return 1";
        echo "    print(lines)";
        echo "    ###############################################";
        echo "    # Part One";
        echo "    print(\"Part 1: \")";
        echo "    ###############################################";
        echo "    # Part Two";
        echo "    print(\"Part 2: \")";
        echo "    return 0";
        echo "";
        echo "";
        echo "main()";
    } > "$SOLVE_FILE" 
else
    echo "Directory $DAY_FOLDER_NAME already exist"
fi

exit 0