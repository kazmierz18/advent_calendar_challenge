"""Day 14 problem solver"""
import re
from collections import defaultdict

def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None

class Chemical:
    def __init__(self, name, quantity):
        self.name=name
        self.quantity=quantity

class Reaction:
    def __init__(self, source, target):
        self.sources=[]
        for s in source:
            self.sources.append(Chemical(s[1],int(s[0])))
        self.target=Chemical(target[1], int(target[0]))

    def list_ingredients_for(self, desired_quantity):
        mul=desired_quantity//self.target.quantity
        if desired_quantity%self.target.quantity:
            mul+=1
        ingredients=[]
        for s in self.sources:
            ingredients.append(Chemical(s.name, s.quantity*mul))
        product=Chemical(self.target.name, self.target.quantity*mul)
        return (ingredients, product)

def main():
    lines = read_lines("Day_14/inputs/input2")
    if lines is None:
        return 1
    reactions = defaultdict(list)
    for line in lines:
        m=re.findall(r'(\d+)\s(\w+)',line)
        reactions[m[-1][1]]=Reaction(m[:-1],m[-1])
    ###############################################
    # Part One
    fuel=reactions['FUEL']
    ingreadients, product=fuel.list_ingredients_for(1)
    for i in ingreadients:
        ingr, pro=reactions[i.name].list_ingredients_for(i.quantity)

    print("Part 1: ")
    ###############################################
    # Part Two
    print("Part 2: ")
    return 0


main()
