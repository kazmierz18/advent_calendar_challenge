"""Day 3 problem solver"""

from collections import namedtuple


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


Point = namedtuple('Point', ['x', 'y'])


def main():
    lines = read_lines("Day_3/inputs/input1")
    if lines is None:
        return 1
    ###############################################
    # Part One
    print("Part 1: ")
    slope = Point(3, 1)
    slopLen = len(lines)
    slopeWidth = len(lines[0])
    position = Point(0, 0)
    cnt = 0
    while position.y < slopLen:
        x = position.x % slopeWidth
        y = position.y
        if lines[y][x] == '#':
            cnt += 1
        position = Point(position.x+slope.x, position.y+slope.y)
    print(cnt)
    ###############################################
    # Part Two
    print("Part 2: ")
    slopes = [Point(1, 1), Point(3, 1), Point(5, 1), Point(7, 1), Point(1, 2)]
    ret = 1
    for slope in slopes:
        position = Point(0, 0)
        cnt = 0
        while position.y < slopLen:
            x = position.x % slopeWidth
            y = position.y
            if lines[y][x] == '#':
                cnt += 1
            position = Point(position.x+slope.x, position.y+slope.y)
        ret *= cnt
    print(ret)
    ###############################################
    return 0


main()
