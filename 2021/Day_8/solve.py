"""Day 8 problem solver"""

from functools import reduce


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


class Digit():
    def __init__(self, offset):
        self.value = ' '
        self.offset = offset*8

    def __segmentA(self, display, c):
        for i in range(4):
            display[1][self.offset+1+i] = c

    def __segmentD(self, display, c):
        for i in range(4):
            display[4][self.offset+1+i] = c

    def __segmentG(self, display, c):
        for i in range(4):
            display[7][self.offset+1+i] = c

    def __segmentB(self, display, c):
        display[2][self.offset] = c
        display[3][self.offset] = c

    def __segmentC(self, display, c):
        display[2][self.offset+5] = c
        display[3][self.offset+5] = c

    def __segmentE(self, display, c):
        display[5][self.offset] = c
        display[6][self.offset] = c

    def __segmentF(self, display, c):
        display[5][self.offset+5] = c
        display[6][self.offset+5] = c

    def shadow(self, display):
        self.__segmentA(display, '.')
        self.__segmentB(display, '.')
        self.__segmentC(display, '.')
        self.__segmentD(display, '.')
        self.__segmentE(display, '.')
        self.__segmentF(display, '.')
        self.__segmentG(display, '.')

    def show(self, segments, display):
        for segment in segments:
            if segment == 'a':
                self.__segmentA(display, 'a')
            elif segment == 'b':
                self.__segmentB(display, 'b')
            elif segment == 'c':
                self.__segmentC(display, 'c')
            elif segment == 'd':
                self.__segmentD(display, 'd')
            elif segment == 'e':
                self.__segmentE(display, 'e')
            elif segment == 'f':
                self.__segmentF(display, 'f')
            elif segment == 'g':
                self.__segmentG(display, 'g')


class Display():
    def __init__(self):
        self.digits = [Digit(0), Digit(1), Digit(2), Digit(3)]
        self.display = [[' ' for _ in range(8 * 4)] for _ in range(8)]
        for digit in self.digits:
            digit.shadow(self.display)

    def clear(self):
        for y in range(len(self.display)):
            for x in range(len(self.display[0])):
                self.display[y][x] = ' '

    def print(self):
        for y in range(len(self.display)):
            for x in range(len(self.display[0])):
                print(self.display[y][x], end='')
            print()
        print()

    def show(self, digit_strings):
        self.clear()
        dd = zip(digit_strings, self.digits)
        for d in dd:
            d[1].show(d[0], self.display)
        self.print()


def isOne(inp):
    return len(inp) == 2


def isFour(inp):
    return len(inp) == 4


def isSeven(inp):
    return len(inp) == 3


def isEight(inp):
    return len(inp) == 7


def isSimpleDigit(inp):
    return isOne(inp) or isFour(inp) or isSeven(inp) or isEight(inp)


def isSevenIn(seven, inp):
    return seven[0] in inp and seven[1] in inp and seven[2] in inp


def isThreeIn(three, inp):
    return three[0] in inp and three[1] in inp and three[2] in inp and three[3] in inp and three[4] in inp


def toNumber(inp):
    if inp == 'abcefg':
        return 0
    elif inp == 'cf':
        return 1
    elif inp == 'acdeg':
        return 2
    elif inp == 'acdfg':
        return 3
    elif inp == 'bcdf':
        return 4
    elif inp == 'abdfg':
        return 5
    elif inp == 'abdefg':
        return 6
    elif inp == 'acf':
        return 7
    elif inp == 'abcdefg':
        return 8
    elif inp == 'abcdfg':
        return 9


def main():
    lines = read_lines("Day_8/inputs/input1")
    if lines is None:
        return 1
    dsp = Display()
    ###############################################
    # Part One
    ret = 0
    for line in lines:
        inp_str, out_str = line.split('|')
        # input_string =list(map(lambda x:''.join(sorted(x)), inp_str.strip().split(' ')))
        output_string = list(map(lambda x: ''.join(
            sorted(x)), out_str.strip().split(' ')))
        ret += len(list(filter(isSimpleDigit, output_string)))
        # dsp.show(digits_string)
    # dsp.print()
    print(f"Part 1: {ret}")
    ###############################################
    # Part Two
    ret = 0
    for line in lines:
        inp_str, out_str = line.split('|')
        input_string = sorted(
            list(map(lambda x: ''.join(sorted(x)), inp_str.strip().split(' '))), key=len)
        output_string = list(map(lambda x: ''.join(
            sorted(x)), out_str.strip().split(' ')))
        translation = {'a': '', 'b': '', 'c': '',
                       'd': '', 'e': '', 'f': '', 'g': ''}
        seven = ''
        for o in input_string:
            if isOne(o):
                translation[o[0]] = 'cf'
                translation[o[1]] = 'cf'
            if isSeven(o):
                seven = o
                for c in o:
                    if translation[c] == '':
                        translation[c] = 'a'
                        break
            if isFour(o):
                for c in o:
                    if translation[c] == '':
                        translation[c] = 'bd'
                        translation[c] = 'bd'
            if len(o) == 5:
                #  2 3 5
                if isSevenIn(seven, o):
                    three = o
                    # this is 3
                    for c in o:
                        if translation[c] == '':
                            translation[c] = 'dg'
                            translation[c] = 'dg'
                    for x in translation:
                        if translation[x] == '':
                            translation[x] = 'e'

            if len(o) == 6:
                #   0  6  9
                if isThreeIn(three, o):
                    # this is 9
                    tmp = o
                    for x in three:
                        tmp = tmp.replace(x, '')
                    translation[tmp] = 'b'
                    for x in translation:
                        if translation[x] == 'bd':
                            translation[x] = 'd'
                    for x in translation:
                        if translation[x] == 'dg':
                            translation[x] = 'g'
                elif not isSevenIn(seven, o):
                    # this is 6
                    for c in ['a', 'b', 'c', 'd', 'e', 'f', 'g']:
                        if c not in o:
                            translation[c] = 'c'
                            break
                    for x in translation:
                        if translation[x] == 'cf':
                            translation[x] = 'f'

        translated = list(map(lambda out: ''.join(sorted(
            translation[x] for x in out)), output_string))
        val = 0
        for x in translated:
            val = 10*val + toNumber(x)
        # dsp.show(translated)
        # print(val)
        ret += val

        # dsp.show(digits_string)
    print(f"Part 2: {ret}")
    return 0


main()
