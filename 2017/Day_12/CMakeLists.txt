project(Day_12)

set(CMAKE_CXX_COMPILER clang++)
set(CMAKE_CXX_CLANG_TIDY clang-tidy -checks=-*,readability-*)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_FLAGS_DEBUG " -Wall -O0 -g3 -Wpedantic")
set(CMAKE_CXX_FLAGS_RELEASE " -Wall -O2")

add_executable(solve main.cpp)
