"""Day 20 problem solver"""

from collections import defaultdict

def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def main():
    lines = read_lines("Day_20/inputs/input1")
    if lines is None:
        return 1

    directions = {
        "N": (0, -1),
        "E": (1, 0),
        "S": (0, 1),
        "W": (-1, 0)
    }
    positions = []
    x = 1000
    y = 1000
    m = defaultdict(set)
    prev_x, prev_y = x, y
    distances = defaultdict(int)

    for c in lines[0]:
        if c == "(":
            positions.append((x, y))
        elif c == ")":
            x, y = positions.pop()
        elif c == "|":
            x, y = positions[-1]
        else:
            dx, dy = directions[c]
            x += dx
            y += dy
            m[(x, y)].add((prev_x, prev_y))
            if distances[(x, y)] != 0:
                distances[(x, y)] = min(distances[(x, y)],
                                        distances[(prev_x, prev_y)]+1)
            else:
                distances[(x, y)] = distances[(prev_x, prev_y)]+1
        prev_x, prev_y = x, y

    ###############################################
    # Part One
    print("Part 1: ", max(distances.values()))
    ###############################################
    # Part Two
    print("Part 2: ", len([x for x in distances.values() if x >= 1000]))
    return 0


main()
