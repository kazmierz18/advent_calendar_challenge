"""Day 22 problem solver"""

import re
import numpy as np
import matplotlib.pyplot as plt
import heapq
from dataclasses import dataclass, field
from enum import IntEnum

class LandType(IntEnum):
    ROCKY = 0
    WETLANDS = 1
    NARROW = 2

class Equ(IntEnum):
    EMPTY_HAND = 0
    TORCH = 1
    CLIMBING_GEAR = 2

valid_items = {LandType.ROCKY: (Equ.TORCH, Equ.CLIMBING_GEAR),
               LandType.WETLANDS: (Equ.EMPTY_HAND, Equ.CLIMBING_GEAR),
               LandType.NARROW: (Equ.TORCH, Equ.EMPTY_HAND)}

NEIGHBORS = [(0, -1, Equ.TORCH), (0, 1, Equ.TORCH),
             (-1, 0, Equ.TORCH), (1, 0, Equ.TORCH),
             (0, -1, Equ.EMPTY_HAND), (0, 1, Equ.EMPTY_HAND),
             (-1, 0, Equ.EMPTY_HAND), (1, 0, Equ.EMPTY_HAND),
             (0, -1, Equ.CLIMBING_GEAR), (0, 1, Equ.CLIMBING_GEAR),
             (-1, 0, Equ.CLIMBING_GEAR), (1, 0, Equ.CLIMBING_GEAR)]


@dataclass
class Node:
    x: int
    y: int
    e: int
    parent: any
    g: int =field(default=0)
    f: int =field(default=0)
    hash: int =field(default=0)

    def equal(self, other):
        return (self.x == other.x and self.y == other.y and self.e == other.e)

    def __gt__(self, other):
        return self.f > other.f

    def __lt__(self, other):
        return self.f < other.f

    def __hash__(self):
        if self.hash == 0:
            self.hash = hash(self.x) ^ hash(self.y) ^ hash(self.e) ^ hash(
                self.g) ^ (self.parent.__hash__() if self.parent else hash(None))
        return self.hash

    def distance(self, target):
        return abs(self.x-target.x)+abs(self.y-target.y)


def a_star(cave, map, start, end):
    helper = np.full([3, cave.shape[0], cave.shape[1]], 2 << 32)

    start_node = Node(start[0], start[1], start[2], None)
    end_node = Node(end[0], end[1], end[2], None)
    open_heap = []

    heapq.heappush(open_heap, start_node)
    closed_set = set()
    while open_heap:
        print("Len %d %d"%(len(open_heap),len(closed_set)))
        current = heapq.heappop(open_heap)
        if current.equal(end_node):
            return current

        closed_set.add(current)

        for dx, dy, equ in NEIGHBORS:
            px = current.x + dx
            py = current.y + dy
            # Make sure within range and valid equ for current and next
            if (px > cave.shape[1]-1 or px < 0 or py > cave.shape[0]-1 or
                    py < 0 or equ not in valid_items[cave[py][px]] or
                    equ not in valid_items[cave[current.y][current.x]]):
                continue

            child = Node(px, py, equ, current)

            if child in closed_set:
                continue
            child.g = current.g + 1 + \
                (7 if child.e != child.parent.e else 0)
            child.f = child.g + child.distance(end_node)

            if child.g >= helper[child.e][child.y][child.x]:
                continue
            if child not in open_heap:
                helper[child.e][child.y][child.x] = child.g
                map[child.y][child.x] = child.g
                heapq.heappush(open_heap, child)


def read_lines(filename):
    "Read input data"
    with open(filename) as file:
        lines = file.readlines()
        return [l.rstrip('\r\n') for l in lines]
    return None


def main():
    lines = read_lines("Day_22/inputs/input1")
    if lines is None:
        return 1
    m = re.findall("([0-9]+)", lines[0])
    depth = int(m[0])
    m = re.findall("([0-9]+)", lines[1])
    target = (int(m[0]), int(m[1]))
    cave = np.zeros([target[1]+50, target[0]+50], dtype=int)
    erosions = np.zeros(cave.shape, dtype=int)

    for y in range(cave.shape[0]):
        for x in range(cave.shape[1]):
            if x == 0 and y == 0 or x == target[0] and y == target[1]:
                erosions[y][x] = (0+depth) % 20183
            elif y == 0:
                erosions[y][x] = (x*16807+depth) % 20183
            elif x == 0:
                erosions[y][x] = (y*48271+depth) % 20183
            else:
                erosions[y][x] = (erosions[y][x-1] *
                                  erosions[y-1][x]+depth) % 20183
            cave[y][x] = erosions[y][x] % 3
    ###############################################
    # Part One
    sum = 0
    for y in range(target[1]+1):
        for x in range(target[0]+1):
            sum += cave[y][x]
    print("Part 1: ", sum)
    ###############################################
    # Part Two
    map = np.zeros(cave.shape)

    b = a_star(cave, map, (0, 0, Equ.TORCH), (target[0], target[1], Equ.TORCH))

    # n = b
    # while n:
    #     map[n.y][n.x] = b.g
    #     n = n.parent
    # f = plt.figure()
    # f.add_subplot(1, 2, 1)
    # plt.imshow(cave)
    # f.add_subplot(1, 2, 2)
    # plt.imshow(map)
    # plt.show()
    print("Part 2: ", b.g)
    return 0


main()
