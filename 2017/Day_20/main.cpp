/*===============*/
/* Day 20        */
/*===============*/
#include <array>
#include <cmath>
#include <fstream>
#include <iostream>
#include <regex>
#include <string>
#include <vector>

struct vec {
  int x = 0;
  int y = 0;
  int z = 0;
  vec(int const x, int const y, int const z) : x(x), y(y), z(z) {}
  double mod() const { return std::sqrt(x * x + y * y + z * z); }
  bool operator==(vec const &other) const {
    return other.x == x and other.y == y and other.z == z;
  }
  void operator+=(vec const &other) {
    x += other.x;
    y += other.y;
    z += other.z;
  }
  vec operator-(vec const &other) const {
    return {x - other.x, y - other.y, z - other.z};
  }
};

struct particle {
  bool crashed = false;
  int id;
  vec position;
  vec velocity;
  vec acceleration;
  particle(vec const p, vec const v, vec const a, int const id)
      : id(id), position(p), velocity(v), acceleration(a) {}
  bool collided(particle const &other) const {
    if (&other == this) {
      return false;
    }
    return other.position == position;
  }
  void simulate() {
    velocity += acceleration;
    position += velocity;
  }
  int distance_manhattan(particle const &other) const {
    auto const tmp = position - other.position;
    return std::abs(tmp.x) + std::abs(tmp.y) + std::abs(tmp.z);
  }
};

int main(int argc, char **argv) {
  // read input file
  char *name = nullptr;
  if (argc >= 2) {
    name = argv[1];
  } else {
    return 1;
  }
  std::ifstream file(name);
  std::string tmp;
  std::vector<std::string> lines;
  while (std::getline(file, tmp)) {
    // remove new line characters
    if (*(tmp.end()) == '\n') {
      tmp.pop_back();
    }
    lines.push_back(tmp);
  }
  //================================================
  std::regex const re("-?\\d+");
  std::vector<particle> particles;
  particles.reserve(lines.size());
  int id = 0;
  for (auto const &line : lines) {
    std::string input = line;
    std::smatch sMatch;
    std::array<int, 9> values;
    int i = 0;
    while (i < values.size() and std::regex_search(input, sMatch, re)) {
      values[i] = std::atoi(sMatch[0].str().c_str());
      i++;
      input = sMatch.suffix().str();
    }
    particles.emplace_back(vec(values[0], values[1], values[2]),
                           vec(values[3], values[4], values[5]),
                           vec(values[6], values[7], values[8]), id);
    id++;
  }
  std::sort(particles.begin(), particles.end(),
            [](auto const &lhs, auto const &rhs) {
              return lhs.acceleration.mod() < rhs.acceleration.mod();
            });
  std::cout << "Part One: " << particles[0].id << "\n";
  //================================================
  auto i = 0U;
  auto end = particles.end();
  while (i++ < 20000) {
    for (auto &p : particles) {
      p.simulate();
    }
    for (auto p1 = particles.begin(); p1 != (end - 1); ++p1) {
      for (auto p2 = p1 + 1; p2 != end; ++p2) {
        if ((*p1).collided(*p2)) {
          (*p1).crashed = true;
          (*p2).crashed = true;
        }
      }
    }
    end = std::remove_if(particles.begin(), end,
                         [](auto const &p) { return p.crashed; });
  }

  std::cout << "Part Two: " << end - particles.begin() << "\n";
  return 0;
}
